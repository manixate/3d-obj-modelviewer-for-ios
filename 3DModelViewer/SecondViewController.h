//
//  SecondViewController.h
//  3DModelViewer
//
//  Created by Muhammad Azeem on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"
@interface SecondViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *models;
}

@property (unsafe_unretained, nonatomic) IBOutlet UITableView *tableView;

@end
