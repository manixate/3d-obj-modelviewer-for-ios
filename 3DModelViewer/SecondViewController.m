//
//  SecondViewController.m
//  3DModelViewer
//
//  Created by Muhammad Azeem on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController
@synthesize tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"OpenGLES2.0", @"OpenGLES2.0");
        self.tabBarItem.image = [UIImage imageNamed:@"second"];
    }
    return self;
}
							
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    if (!models)
        models = [NSMutableArray arrayWithObjects:@"Ninja", @"micronapalmv2", @"Table1", @"Chair", @"audi", @"jeep", @"table", @"cube", @"rapide", @"building3-tri", @"edifici_prat-tri", @"bridge-tri", @"industrial_building4-tri", @"industrial_building-tri", @"Pub_&_Tenement-tri", @"LongHouse-tri", @"skyscraper4-tri", @"Shop_w-tri", @"World", nil];
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
  self.tabBarController.title = @"OpenGL ES 2.0";
}

-(int)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return  1;
}
-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  models.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"myCell"];
    cell.textLabel.text = [models objectAtIndex:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailViewController* detailViewController = [[DetailViewController alloc]init];
    detailViewController.renderingEngineAPI = kEAGLRenderingAPIOpenGLES2;
    detailViewController.modelName = [models objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:detailViewController animated:YES];
    
    UITableViewCell* cell = [tableView1 cellForRowAtIndexPath:indexPath];
    [cell setSelected:NO animated:YES];
}
@end
