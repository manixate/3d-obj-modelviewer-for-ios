//
//  Shader.fsh
//  BumpMap
//
//  Created by Muhammad Azeem on 7/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

varying lowp vec4 colorVarying;

varying mediump vec2 TexCoordOut; // New
uniform sampler2D Texture; // New
uniform bool hasTexture;

void main()
{
    if (hasTexture == true)
    {
        lowp vec4 texColor = texture2D(Texture, TexCoordOut); // New
        gl_FragColor = colorVarying * texColor;
    }
    else
    {
        gl_FragColor = colorVarying;
    }
}