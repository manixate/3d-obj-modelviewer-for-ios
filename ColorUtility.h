//
//  ColorUtility.h
//  ObjModelViewer
//
//  Created by Muhammad Azeem on 7/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef ObjModelViewer_ColorUtility_h
#define ObjModelViewer_ColorUtility_h

const static GLfloat sunPos[]={0.0, 0.0, 1.0, 0.0};
const static GLfloat posFill1[]={-15.0,15.0,0.0,1.0};
const static GLfloat posFill2[]={-10.0,-4.0,1.0,1.0};
const static GLfloat white[]={1.0,1.0,1.0,1.0};
const static GLfloat dimWhite[]={0.25,0.25,0.25, 1.0};
const static GLfloat dimblue[]={0.0,0.0,.2,1.0};
const static GLfloat cyan[]={0.0,1.0,1.0,1.0};
const static GLfloat yellow[]={1.0,1.0,0.0,1.0};
const static GLfloat dimmagenta[]={.75,0.0,.25,1.0};
const static GLfloat dimcyan[]={0.0,.5,.5,1.0};
const static GLfloat paleYellow[]={1.0,1.0,0.3,1.0};
const static GLfloat black[]={0.0,0.0,0.0,0.0};
const static GLfloat darkGray[] = {0.2, 0.2, 0.2, 1.0};

#endif
