//
//  Cube.h
//  ObjModelViewer
//
//  Created by Muhammad Azeem on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef ObjModelViewer_Cube_h
#define ObjModelViewer_Cube_h

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

#define TEX_COORD_MAX 4

static const GLfloat gCubeVertexData[288] = 
{
    // Data layout for each line below is:
    // positionX, positionY, positionZ,     normalX, normalY, normalZ,
    0.5f, -0.5f, -0.5f,        1.0f, 0.0f, 0.0f,        TEX_COORD_MAX, 0.0f,
    0.5f, 0.5f, -0.5f,         1.0f, 0.0f, 0.0f,        TEX_COORD_MAX, TEX_COORD_MAX,
    0.5f, -0.5f, 0.5f,         1.0f, 0.0f, 0.0f,        0.0f, 0.0f,
    0.5f, -0.5f, 0.5f,         1.0f, 0.0f, 0.0f,        0.0f, 0.0f,
    0.5f, 0.5f, -0.5f,         1.0f, 0.0f, 0.0f,        TEX_COORD_MAX, TEX_COORD_MAX,
    0.5f, 0.5f, 0.5f,          1.0f, 0.0f, 0.0f,        0.0f, TEX_COORD_MAX,
    
    0.5f, 0.5f, -0.5f,         0.0f, 1.0f, 0.0f,        TEX_COORD_MAX, 0.0f,
    -0.5f, 0.5f, -0.5f,        0.0f, 1.0f, 0.0f,        TEX_COORD_MAX, TEX_COORD_MAX,
    0.5f, 0.5f, 0.5f,          0.0f, 1.0f, 0.0f,        0.0f, 0.0f,
    0.5f, 0.5f, 0.5f,          0.0f, 1.0f, 0.0f,        0.0f, 0.0f,
    -0.5f, 0.5f, -0.5f,        0.0f, 1.0f, 0.0f,        TEX_COORD_MAX, TEX_COORD_MAX,
    -0.5f, 0.5f, 0.5f,         0.0f, 1.0f, 0.0f,        0.0f, TEX_COORD_MAX,
    
    -0.5f, 0.5f, -0.5f,        -1.0f, 0.0f, 0.0f,        TEX_COORD_MAX, 0.0f,
    -0.5f, -0.5f, -0.5f,       -1.0f, 0.0f, 0.0f,        TEX_COORD_MAX, TEX_COORD_MAX,
    -0.5f, 0.5f, 0.5f,         -1.0f, 0.0f, 0.0f,        0.0f, 0.0f,
    -0.5f, 0.5f, 0.5f,         -1.0f, 0.0f, 0.0f,        0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,       -1.0f, 0.0f, 0.0f,        TEX_COORD_MAX, TEX_COORD_MAX,
    -0.5f, -0.5f, 0.5f,        -1.0f, 0.0f, 0.0f,        0.0f, TEX_COORD_MAX,
    
    -0.5f, -0.5f, -0.5f,       0.0f, -1.0f, 0.0f,        TEX_COORD_MAX, 0.0f,
    0.5f, -0.5f, -0.5f,        0.0f, -1.0f, 0.0f,        TEX_COORD_MAX, TEX_COORD_MAX,
    -0.5f, -0.5f, 0.5f,        0.0f, -1.0f, 0.0f,        0.0f, 0.0f,
    -0.5f, -0.5f, 0.5f,        0.0f, -1.0f, 0.0f,        0.0f, 0.0f,
    0.5f, -0.5f, -0.5f,        0.0f, -1.0f, 0.0f,        TEX_COORD_MAX, TEX_COORD_MAX,
    0.5f, -0.5f, 0.5f,         0.0f, -1.0f, 0.0f,        0.0f, TEX_COORD_MAX,
    
    0.5f, 0.5f, 0.5f,          0.0f, 0.0f, 1.0f,        TEX_COORD_MAX, 0.0f,
    -0.5f, 0.5f, 0.5f,         0.0f, 0.0f, 1.0f,        TEX_COORD_MAX, TEX_COORD_MAX,
    0.5f, -0.5f, 0.5f,         0.0f, 0.0f, 1.0f,        0.0f, 0.0f,
    0.5f, -0.5f, 0.5f,         0.0f, 0.0f, 1.0f,        0.0f, 0.0f,
    -0.5f, 0.5f, 0.5f,         0.0f, 0.0f, 1.0f,        TEX_COORD_MAX, TEX_COORD_MAX,
    -0.5f, -0.5f, 0.5f,        0.0f, 0.0f, 1.0f,        0.0f, TEX_COORD_MAX,
    
    0.5f, -0.5f, -0.5f,        0.0f, 0.0f, -1.0f,        TEX_COORD_MAX, 0.0f,
    -0.5f, -0.5f, -0.5f,       0.0f, 0.0f, -1.0f,        TEX_COORD_MAX, TEX_COORD_MAX,
    0.5f, 0.5f, -0.5f,         0.0f, 0.0f, -1.0f,        0.0f, 0.0f,
    0.5f, 0.5f, -0.5f,         0.0f, 0.0f, -1.0f,        0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,       0.0f, 0.0f, -1.0f,        TEX_COORD_MAX, TEX_COORD_MAX,
    -0.5f, 0.5f, -0.5f,        0.0f, 0.0f, -1.0f,         0.0f, TEX_COORD_MAX
};

#endif
