//
//  ViewController.h
//  ObjModelViewer
//
//  Created by Muhammad Azeem on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GLView;

@interface DetailViewController : UIViewController
{
    int renderingEngineAPI;
    GLView *glView;
    NSString *modelName;
}
@property (nonatomic) int renderingEngineAPI;
@property (nonatomic) NSString *modelName;
@end
