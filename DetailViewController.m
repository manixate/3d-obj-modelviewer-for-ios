//
//  ViewController.m
//  ObjModelViewer
//
//  Created by Muhammad Azeem on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DetailViewController.h"
#import "GLView.h"
#import <QuartzCore/QuartzCore.h>
#import "Model3D.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

@synthesize renderingEngineAPI, modelName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
//    GLView *glView = [[GLView alloc] initWithFrame:[UIScreen mainScreen].bounds andRenderingAPI:renderingEngine];
    
    self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];   
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (modelName == nil)
    {
        glView = [[GLView alloc] initWithFrame:self.view.frame andRenderingAPI:renderingEngineAPI];
    }
    else
    {        
        glView = [[GLView alloc] initWithFrame:self.view.frame renderingAPI:renderingEngineAPI andModelName:modelName];
    }
    
    [self.view addSubview:glView];
}
-(void)viewWillAppear:(BOOL)animated
{
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [glView removeFromSuperview];
    [glView stopRendering];
}

@end
