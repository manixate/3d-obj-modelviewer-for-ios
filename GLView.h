//
//  GLView.h
//  ObjModelViewer
//
//  Created by Muhammad Azeem on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RenderingEngine.h"
#import <QuartzCore/QuartzCore.h>

@interface GLView : UIView
{
    EAGLContext *mContext;
    RenderingEngine *mRenderingEngine;
    NSTimeInterval mTimestamp;
    NSTimeInterval mElapsed;
    
    EAGLRenderingAPI mRenderingApi;
    CADisplayLink *displayLink;
    
    CGPoint currentTouchPoint;
    
    bool supportMSAA;
}

@property (nonatomic) RenderingEngine *mRenderingEngine;

- (id)initWithFrame:(CGRect)frame andRenderingAPI:(EAGLRenderingAPI)anApi;
- (id)initWithFrame:(CGRect)frame renderingAPI:(EAGLRenderingAPI)anApi andModelName:(NSString *)modelName;

- (EAGLContext *)createContextFromLayer:(CAEAGLLayer *)aLayer;

- (void)drawView:(CADisplayLink *) displayLink;

- (void)stopRendering;

- (void)tapGestureAction:(UITapGestureRecognizer *)gesture;
- (void)panGestureAction:(UIPanGestureRecognizer *)gesture;
- (void)pinchGestureAction:(UIPinchGestureRecognizer *)gesture;

@end