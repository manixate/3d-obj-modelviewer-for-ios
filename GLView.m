//
//  GLView.m
//  ObjModelViewer
//
//  Created by Muhammad Azeem on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GLView.h"
#import "RenderingEngineES1.h"
#import "RenderingEngineES2.h"

@implementation GLView
@synthesize mRenderingEngine;

+ (Class) layerClass
{
    return [CAEAGLLayer class];
}

- (id)initWithFrame:(CGRect)frame andRenderingAPI:(EAGLRenderingAPI)anApi
{
    self = [super initWithFrame:frame];
    if (self)
    {
        mRenderingApi = anApi;
        
        mContext = [self createContextFromLayer: (CAEAGLLayer *)super.layer];
        if (mContext == nil || ![EAGLContext setCurrentContext: mContext])
            return nil;
        
        if (mRenderingApi == kEAGLRenderingAPIOpenGLES1)
        {
            NSLog(@"Using OpenGL ES 1.1");
            mRenderingEngine = [[RenderingEngineES1 alloc] init];
        }
        else
        {
            NSLog(@"Using OpenGL ES 2.0");
            mRenderingEngine = [[RenderingEngineES2 alloc] init];
        }
        
        [mContext renderbufferStorage:GL_RENDERBUFFER fromDrawable: (CAEAGLLayer *)super.layer];
        [mRenderingEngine initFrameBuffer];
        
        mTimestamp = CACurrentMediaTime();
        displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(drawView:)];
        
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureAction:)];
        [self addGestureRecognizer:panGesture];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
        tapGesture.numberOfTapsRequired = 2;
        [self addGestureRecognizer:tapGesture];
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame renderingAPI:(EAGLRenderingAPI)anApi andModelName:(NSString *)modelName
{
    self = [super initWithFrame:frame];
    if (self)
    {
        mRenderingApi = anApi;
        
        mContext = [self createContextFromLayer: (CAEAGLLayer *)super.layer];
        if (mContext == nil || ![EAGLContext setCurrentContext: mContext])
            return nil;
        
        if ([modelName isEqualToString:@"World"])
        {
            NSArray *modelNames = [NSArray arrayWithObjects:@"cube", @"building3-tri", @"building3-tri", @"building3-tri", @"building3-tri", @"building3-tri", @"building3-tri", @"building3-tri", nil];
            
            Vector3 positions[] = {Vector3Make(0, -2, 0), Vector3Make(0, -0.5, 0), Vector3Make(1, -0.5, 0), Vector3Make(-1, -0.5, 0), Vector3Make(-0.5, -0.5, -0.5), Vector3Make(0.5, -0.5, 0.5), Vector3Make(-0.5, -0.5, 1.1), Vector3Make(0.5, -0.5, 1.1)};
            Vector3 scales[] = {Vector3Make(1.5, 1, 2.5), Vector3Make(0.5, 0.5, 0.5), Vector3Make(0.5, 0.5, 0.5), Vector3Make(0.5, 0.5, 0.5), Vector3Make(0.5, 0.5, 0.5), Vector3Make(0.5, 0.5, 0.5), Vector3Make(0.5, 0.5, 0.5), Vector3Make(0.5, 0.5, 0.5), Vector3Make(0.5, 0.5, 0.5)};
            
            if (mRenderingApi == kEAGLRenderingAPIOpenGLES1)
            {
                NSLog(@"Using OpenGL ES 1.1");
                
                mRenderingEngine = [[RenderingEngineES1 alloc] initWithModelNames:modelNames positions:positions scales:scales];
            }
            else
            {
                NSLog(@"Using OpenGL ES 2.0");
                
                mRenderingEngine = [[RenderingEngineES2 alloc] initWithModelNames:modelNames positions:positions scales:scales];
            }
        }
        else
        {
            NSArray *modelNames = [NSArray arrayWithObjects:modelName, nil];
            
            Vector3 positions[] = {Vector3Make(0, 0, 0)};
            Vector3 scales[] = {Vector3Make(1, 1, 1)};
            
            if (mRenderingApi == kEAGLRenderingAPIOpenGLES1)
            {
                NSLog(@"Using OpenGL ES 1.1");
                
                mRenderingEngine = [[RenderingEngineES1 alloc] initWithModelNames:modelNames positions:positions scales:scales];
            }
            else
            {
                NSLog(@"Using OpenGL ES 2.0");
                
                mRenderingEngine = [[RenderingEngineES2 alloc] initWithModelNames:modelNames positions:positions scales:scales];
            }
        }
        
        [mContext renderbufferStorage:GL_RENDERBUFFER fromDrawable: (CAEAGLLayer *)super.layer];
        [mRenderingEngine initFrameBuffer];
        
        mTimestamp = CACurrentMediaTime();
        displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(drawView:)];
        
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureAction:)];
        [self addGestureRecognizer:panGesture];
        
        UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGestureAction:)];
        [self addGestureRecognizer:pinchGesture];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
        tapGesture.numberOfTapsRequired = 2;
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

#pragma mark - Own Methods

- (EAGLContext *)createContextFromLayer:(CAEAGLLayer *)aLayer
{
    aLayer.opaque = YES;
    
    EAGLContext *context = [[EAGLContext alloc] initWithAPI:mRenderingApi];
    
    if (!context)
    {
        NSLog(@"FAILED: Cannot Create Context");
        
        return nil;
    }
    return context;
}

- (void)drawView:(CADisplayLink *)displayLink1
{
    if (displayLink1 != nil)
    {
        NSTimeInterval elapsed = displayLink1.timestamp - mTimestamp;
        mTimestamp = displayLink1.timestamp;
        mElapsed = elapsed;
        [mRenderingEngine update:elapsed];//WithScale:1.001 rotation:CGPointMake(0, 0) translation:Vector3Make(0, 0, 0) andTimeStamp:elapsed];
    }
    
    [mRenderingEngine render];
    
    [mContext presentRenderbuffer:GL_RENDERBUFFER];
}

- (void)stopRendering
{
    [displayLink invalidate];
}

#pragma mark - Gesture Recognizer Methods

- (void)tapGestureAction:(UITapGestureRecognizer *)gesture
{
    NSLog(@"In Tap Gesture Action");
    [mRenderingEngine toggleMSAA];
}

- (void) panGestureAction:(UIPanGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan)
    {
        currentTouchPoint = [gesture locationInView:self];
    }
    else if (gesture.state == UIGestureRecognizerStateChanged)
    {
        CGPoint point = [gesture locationInView:self];
        
        if (gesture.numberOfTouches == 2)
        {
            mRenderingEngine.xTranslation += (point.x - currentTouchPoint.x);
            mRenderingEngine.yTranslation += -(point.y - currentTouchPoint.y);
            [mRenderingEngine update:mElapsed];
        }
        else if (gesture.numberOfTouches == 1)
        {
            mRenderingEngine.xRotation += (point.y - currentTouchPoint.y);
            mRenderingEngine.yRotation += (point.x - currentTouchPoint.x);
            [mRenderingEngine update:mElapsed];
        }
        
        currentTouchPoint = point;
    }
    else if (gesture.state == UIGestureRecognizerStateEnded)
    {
        
    }
}

- (void)pinchGestureAction:(UIPinchGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateChanged)
    {
        mRenderingEngine.scaleFactor = gesture.scale;
        [mRenderingEngine update:mElapsed];
        //        [mRenderingEngine updateWithScale:gesture.scale rotation:CGPointMake(0, 0) translation:Vector3Make(0, 0, 0) andTimeStamp:mElapsed];
    }
}

#pragma mark - Touches Event Handlers

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    UITouch *touch = [touches anyObject];
//    currentTouchPoint = [touch locationInView:self];
//    
//    [mRenderingEngine onTouchBegin:currentTouchPoint andNumOfTouches:touches.count];
//}
//
//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    UITouch *touch = [touches anyObject];
//    CGPoint point = [touch locationInView:self];
//    
//    [mRenderingEngine onTouchMovedFromPoint:currentTouchPoint toPoint:point andNumOfTouches:touches.count];
//    
//    currentTouchPoint = point;
//}
//
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    UITouch *touch = [touches anyObject];
//    CGPoint point = [touch locationInView:self];
//    
//    [mRenderingEngine onTouchEnd:point andNumOfTouches:touches.count];
//}

@end