//
//  GLtextureLoader.h
//  OpenGLES13
//
//  Created by AH on 5/8/12.
//  Copyright (c) 2012. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

@interface GLtextureLoader : NSObject
{
    GLuint usedTextureID;
    GLuint textures[27];
}

-(GLuint)loadTexture:(char *)name;

@end
