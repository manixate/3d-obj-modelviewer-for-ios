//
//  GLtextureLoader.m
//  OpenGLES13
//
//  Created by AH on 5/8/12.
//  Copyright (c) 2012. All rights reserved.
//


#import "GLtextureLoader.h"

@implementation GLtextureLoader

-(id)init
{
    if(self = [super init])
    {
        usedTextureID = 0;
        if(usedTextureID<26)
            glGenTextures(26, textures);
        else
            {
                NSLog(@"Excceeded max textures");
            }
    }
    return self;
}

-(GLuint)loadTexture:(char *)name
{
    if(!name)
    {
        NSLog(@"No texture ");
		return 9999;
    }
    NSLog(@"Texture Loading Started");
    NSString *stringName = [NSString stringWithCString:name encoding:NSUTF8StringEncoding];
    UIImage *texImage = [UIImage imageNamed:stringName];
    CGImageRef textureImage = texImage.CGImage;
    if (textureImage == nil) {
        NSLog(@"Failed to load texture image");
		return 9999;
    }
	
    NSInteger texWidth = CGImageGetWidth(textureImage);
    NSInteger texHeight = CGImageGetHeight(textureImage);
	
	GLubyte *textureData = (GLubyte *)malloc(texWidth * texHeight * 4);

    CGContextRef textureContext = CGBitmapContextCreate(textureData,
                                                        texWidth, texHeight,
                                                        8, texWidth * 4,
                                                        CGImageGetColorSpace(textureImage),
                                                        kCGImageAlphaPremultipliedLast);
	CGContextDrawImage(textureContext, CGRectMake(0.0, 0.0, (float)texWidth, (float)texHeight), textureImage);
	CGContextRelease(textureContext);
    
	glGenTextures(1, &usedTextureID);
    
	glBindTexture(GL_TEXTURE_2D, usedTextureID);
    
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData);
	
	free(textureData);

//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT); 	
//    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
    
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//    
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
	//glEnable(GL_TEXTURE_2D);
    NSLog(@"Texture Loading Ended");
    return usedTextureID;
}

@end



