//
//  MaterialStruct.h
//  3DModelViewer
//
//  Created by Muhammad Azeem on 8/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef _DModelViewer_MaterialStruct_h
#define _DModelViewer_MaterialStruct_h

struct Material
{
    char *materialName;
    char *map_Kd;
    char *map_Ka;
    char *map_Ks;
    GLfloat Ka[4];
    GLfloat Ks[4];
    GLfloat Kd[4];
    int illum;
    GLfloat Ns;
    GLfloat Tr;
    
    bool hasTexture;
    GLuint textureID;
};

#endif
