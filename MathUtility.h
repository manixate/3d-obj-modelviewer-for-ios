//
//  MathUtility.h
//  ObjModelViewer
//
//  Created by Muhammad Azeem on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef ObjModelViewer_MathUtility_h
#define ObjModelViewer_MathUtility_h

static float degreesToRadians(float degrees)
{
    return degrees * M_PI / 180;
}

static float radiansToDegree(float radians)
{
    return radians * 180 / M_PI;
}

#endif
