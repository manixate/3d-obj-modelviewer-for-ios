//
//  Matrix3.h
//  ObjModelViewer
//
//  Created by Muhammad Azeem on 7/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Matrix4.h"

#ifndef ObjModelViewer_Matrix3_h
#define ObjModelViewer_Matrix3_h

union _Matrix3
{
    struct
    {
        float m00, m01, m02;
        float m10, m11, m12;
        float m20, m21, m22;
    };
    float m[9];
};
typedef union _Matrix3 Matrix3;

static const Matrix3 Matrix3Identity = {
    1.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f,
    0.0f, 0.0f, 1.0f,
};

static Matrix3 Matrix3InvertAndTranspose(Matrix3 matrix, bool *isInvertible);
static Matrix3 Matrix3Transpose(Matrix3 matrix);
static Matrix3 Matrix3Invert(Matrix3 matrix, bool *isInvertible);
static Matrix3 Matrix3Scale(Matrix3 matrix, float sx, float sy, float sz);
static Matrix3 Matrix4GetMatrix3(Matrix4 matrix);
static Matrix4 Matrix4FromMatrix3(Matrix3 matrix);

static Matrix3 Matrix3Transpose(Matrix3 matrix)
{
    Matrix3 m = { matrix.m[0], matrix.m[3], matrix.m[6],
        matrix.m[1], matrix.m[4], matrix.m[7],
        matrix.m[2], matrix.m[5], matrix.m[8] };
    return m;
}

static Matrix4 Matrix4FromMatrix3(Matrix3 matrix)
{
    Matrix4 m = Matrix4Identity;
    m.m[0] = matrix.m[0];
    m.m[1] = matrix.m[1];
    m.m[2] = matrix.m[2],
    m.m[4] = matrix.m[3];
    m.m[5] = matrix.m[4];
    m.m[6] = matrix.m[5];
    m.m[8] = matrix.m[6];
    m.m[9] = matrix.m[7];
    m.m[10] = matrix.m[8];
    return m;
}

static Matrix3 Matrix3Invert(Matrix3 matrix, bool *isInvertible) {
    GLfloat determinant = (matrix.m00 * (matrix.m11 * matrix.m22 - matrix.m12 * matrix.m21)) + (matrix.m01 * (matrix.m12 * matrix.m20 - matrix.m22 * matrix.m10)) + (matrix.m02 * (matrix.m10 * matrix.m21 - matrix.m11 *matrix.m20));
    bool canInvert = determinant != 0.0f;
    if (isInvertible) {
        *isInvertible = canInvert;
    }
    
    if (!canInvert) {
        return Matrix3Identity;
    }
    
    return Matrix3Scale(Matrix3Transpose(matrix), determinant, determinant, determinant);
}

static Matrix3 Matrix3Scale(Matrix3 matrix, float sx, float sy, float sz)
{
    Matrix3 m = { matrix.m[0] * sx, matrix.m[1] * sx, matrix.m[2] * sx,
        matrix.m[3] * sy, matrix.m[4] * sy, matrix.m[5] * sy,
        matrix.m[6] * sz, matrix.m[7] * sz, matrix.m[8] * sz };
    return m;
}

static Matrix3 Matrix4GetMatrix3(Matrix4 matrix)
{
    Matrix3 m = { matrix.m[0], matrix.m[1], matrix.m[2],
        matrix.m[4], matrix.m[5], matrix.m[6],
        matrix.m[8], matrix.m[9], matrix.m[10] };
    return m;
}

static Matrix3 Matrix3InvertAndTranspose(Matrix3 matrix, bool *isInvertible) {
    return Matrix3Transpose(Matrix3Invert(matrix, isInvertible));
}

#endif
