//
//  Matrix4.h
//  ObjModelViewer
//
//  Created by Muhammad Azeem on 7/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Vector3.h"
#import <QuartzCore/QuartzCore.h>

#ifndef ObjModelViewer_Matrix4_h
#define ObjModelViewer_Matrix4_h

union _Matrix4
{
    struct
    {
        float m00, m01, m02, m03;
        float m10, m11, m12, m13;
        float m20, m21, m22, m23;
        float m30, m31, m32, m33;
    };
    float m[16];
};
typedef union _Matrix4 Matrix4;

static const Matrix4 Matrix4Identity = {
    1.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 1.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 1.0f,
};

static Matrix4 Matrix4MakePerspective(float fovyRadians, float aspect, float nearZ, float farZ);
static Matrix4 Matrix4MakeTranslation(float tx, float ty, float tz);
static Matrix4 Matrix4Rotate(Matrix4 matrix, float radians, float x, float y, float z);
static Matrix4 Matrix4MakeRotation(float radians, float x, float y, float z);
static Matrix4 Matrix4Multiply(Matrix4 matrixLeft, Matrix4 matrixRight);
static Matrix4 Matrix4MakeScale(float sx, float sy, float sz);

static Matrix4 Matrix4MakePerspective(float fovyRadians, float aspect, float nearZ, float farZ)
{
    float cotan = 1.0f / tanf(fovyRadians / 2.0f);
    
    Matrix4 m = { cotan / aspect, 0.0f, 0.0f, 0.0f,
        0.0f, cotan, 0.0f, 0.0f,
        0.0f, 0.0f, (farZ + nearZ) / (nearZ - farZ), -1.0f,
        0.0f, 0.0f, (2.0f * farZ * nearZ) / (nearZ - farZ), 0.0f };
    
    return m;
}

static Matrix4 Matrix4MakeTranslation(float tx, float ty, float tz)
{
    Matrix4 m = Matrix4Identity;
    m.m[12] = tx;
    m.m[13] = ty;
    m.m[14] = tz;
    return m;
}

static Matrix4 Matrix4MakeScale(float sx, float sy, float sz)
{
    Matrix4 m = Matrix4Identity;
    m.m[0] = sx;
    m.m[5] = sy;
    m.m[10] = sz;
    return m;
}

static Matrix4 Matrix4Rotate(Matrix4 matrix, float radians, float x, float y, float z)
{
    Matrix4 rm = Matrix4MakeRotation(radians, x, y, z);
    return Matrix4Multiply(matrix, rm);
}

static Matrix4 Matrix4MakeRotation(float radians, float x, float y, float z)
{
    Vector3 v = Vector3Normalize(Vector3Make(x, y, z));
    float cos = cosf(radians);
    float cosp = 1.0f - cos;
    float sin = sinf(radians);
    
    Matrix4 m = { cos + cosp * v.v[0] * v.v[0],
        cosp * v.v[0] * v.v[1] + v.v[2] * sin,
        cosp * v.v[0] * v.v[2] - v.v[1] * sin,
        0.0f,
        cosp * v.v[0] * v.v[1] - v.v[2] * sin,
        cos + cosp * v.v[1] * v.v[1],
        cosp * v.v[1] * v.v[2] + v.v[0] * sin,
        0.0f,
        cosp * v.v[0] * v.v[2] + v.v[1] * sin,
        cosp * v.v[1] * v.v[2] - v.v[0] * sin,
        cos + cosp * v.v[2] * v.v[2],
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        1.0f };
    
    return m;
}

static Matrix4 Matrix4Multiply(Matrix4 matrixLeft, Matrix4 matrixRight)
{
    Matrix4 m;
    
    m.m[0]  = matrixLeft.m[0] * matrixRight.m[0]  + matrixLeft.m[4] * matrixRight.m[1]  + matrixLeft.m[8] * matrixRight.m[2]   + matrixLeft.m[12] * matrixRight.m[3];
    m.m[4]  = matrixLeft.m[0] * matrixRight.m[4]  + matrixLeft.m[4] * matrixRight.m[5]  + matrixLeft.m[8] * matrixRight.m[6]   + matrixLeft.m[12] * matrixRight.m[7];
    m.m[8]  = matrixLeft.m[0] * matrixRight.m[8]  + matrixLeft.m[4] * matrixRight.m[9]  + matrixLeft.m[8] * matrixRight.m[10]  + matrixLeft.m[12] * matrixRight.m[11];
    m.m[12] = matrixLeft.m[0] * matrixRight.m[12] + matrixLeft.m[4] * matrixRight.m[13] + matrixLeft.m[8] * matrixRight.m[14]  + matrixLeft.m[12] * matrixRight.m[15];
    
    m.m[1]  = matrixLeft.m[1] * matrixRight.m[0]  + matrixLeft.m[5] * matrixRight.m[1]  + matrixLeft.m[9] * matrixRight.m[2]   + matrixLeft.m[13] * matrixRight.m[3];
    m.m[5]  = matrixLeft.m[1] * matrixRight.m[4]  + matrixLeft.m[5] * matrixRight.m[5]  + matrixLeft.m[9] * matrixRight.m[6]   + matrixLeft.m[13] * matrixRight.m[7];
    m.m[9]  = matrixLeft.m[1] * matrixRight.m[8]  + matrixLeft.m[5] * matrixRight.m[9]  + matrixLeft.m[9] * matrixRight.m[10]  + matrixLeft.m[13] * matrixRight.m[11];
    m.m[13] = matrixLeft.m[1] * matrixRight.m[12] + matrixLeft.m[5] * matrixRight.m[13] + matrixLeft.m[9] * matrixRight.m[14]  + matrixLeft.m[13] * matrixRight.m[15];
    
    m.m[2]  = matrixLeft.m[2] * matrixRight.m[0]  + matrixLeft.m[6] * matrixRight.m[1]  + matrixLeft.m[10] * matrixRight.m[2]  + matrixLeft.m[14] * matrixRight.m[3];
    m.m[6]  = matrixLeft.m[2] * matrixRight.m[4]  + matrixLeft.m[6] * matrixRight.m[5]  + matrixLeft.m[10] * matrixRight.m[6]  + matrixLeft.m[14] * matrixRight.m[7];
    m.m[10] = matrixLeft.m[2] * matrixRight.m[8]  + matrixLeft.m[6] * matrixRight.m[9]  + matrixLeft.m[10] * matrixRight.m[10] + matrixLeft.m[14] * matrixRight.m[11];
    m.m[14] = matrixLeft.m[2] * matrixRight.m[12] + matrixLeft.m[6] * matrixRight.m[13] + matrixLeft.m[10] * matrixRight.m[14] + matrixLeft.m[14] * matrixRight.m[15];
    
    m.m[3]  = matrixLeft.m[3] * matrixRight.m[0]  + matrixLeft.m[7] * matrixRight.m[1]  + matrixLeft.m[11] * matrixRight.m[2]  + matrixLeft.m[15] * matrixRight.m[3];
    m.m[7]  = matrixLeft.m[3] * matrixRight.m[4]  + matrixLeft.m[7] * matrixRight.m[5]  + matrixLeft.m[11] * matrixRight.m[6]  + matrixLeft.m[15] * matrixRight.m[7];
    m.m[11] = matrixLeft.m[3] * matrixRight.m[8]  + matrixLeft.m[7] * matrixRight.m[9]  + matrixLeft.m[11] * matrixRight.m[10] + matrixLeft.m[15] * matrixRight.m[11];
    m.m[15] = matrixLeft.m[3] * matrixRight.m[12] + matrixLeft.m[7] * matrixRight.m[13] + matrixLeft.m[11] * matrixRight.m[14] + matrixLeft.m[15] * matrixRight.m[15];
    
    return m;
}
static void Matrix4ToTransform3D(Matrix4 matrix, CATransform3D *transform3D)
{
	transform3D->m11 = (CGFloat)matrix.m[0];
	transform3D->m12 = (CGFloat)matrix.m[1];
	transform3D->m13 = (CGFloat)matrix.m[2];
	transform3D->m14 = (CGFloat)matrix.m[3];
	transform3D->m21 = (CGFloat)matrix.m[4];
	transform3D->m22 = (CGFloat)matrix.m[5];
	transform3D->m23 = (CGFloat)matrix.m[6];
	transform3D->m24 = (CGFloat)matrix.m[7];
	transform3D->m31 = (CGFloat)matrix.m[8];
	transform3D->m32 = (CGFloat)matrix.m[9];
	transform3D->m33 = (CGFloat)matrix.m[10];
	transform3D->m34 = (CGFloat)matrix.m[11];
	transform3D->m41 = (CGFloat)matrix.m[12];
	transform3D->m42 = (CGFloat)matrix.m[13];
	transform3D->m43 = (CGFloat)matrix.m[14];
	transform3D->m44 = (CGFloat)matrix.m[15];
}

static void Transform3DToMatrix4(CATransform3D *transform3D, Matrix4 matrix)
{	
	matrix.m[0] = (GLfloat)transform3D->m11;
	matrix.m[1] = (GLfloat)transform3D->m12;
	matrix.m[2] = (GLfloat)transform3D->m13;
	matrix.m[3] = (GLfloat)transform3D->m14;
	matrix.m[4] = (GLfloat)transform3D->m21;
	matrix.m[5] = (GLfloat)transform3D->m22;
	matrix.m[6] = (GLfloat)transform3D->m23;
	matrix.m[7] = (GLfloat)transform3D->m24;
	matrix.m[8] = (GLfloat)transform3D->m31;
	matrix.m[9] = (GLfloat)transform3D->m32;
	matrix.m[10] = (GLfloat)transform3D->m33;
	matrix.m[11] = (GLfloat)transform3D->m34;
	matrix.m[12] = (GLfloat)transform3D->m41;
	matrix.m[13] = (GLfloat)transform3D->m42;
	matrix.m[14] = (GLfloat)transform3D->m43;
	matrix.m[15] = (GLfloat)transform3D->m44;
}

static Matrix4 Matrix4MakeFromArray(GLfloat m[])
{
    Matrix4 matrix = Matrix4Identity;
    
    matrix.m[0] = m[0];
	matrix.m[1] = m[1];
	matrix.m[2] = m[2];
	matrix.m[3] = m[3];
	matrix.m[4] = m[4];
	matrix.m[5] = m[5];
	matrix.m[6] = m[6];
	matrix.m[7] = m[7];
	matrix.m[8] = m[8];
	matrix.m[9] = m[9];
	matrix.m[10] = m[10];
	matrix.m[11] = m[11];
	matrix.m[12] = m[12];
	matrix.m[13] = m[13];
	matrix.m[14] = m[14];
	matrix.m[15] = m[15];
    
    return matrix;
}

#endif
