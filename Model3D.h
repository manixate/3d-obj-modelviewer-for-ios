//
//  Model3D.h
//  OpenGLES13 ahmad
//
//  Created by AH on 5/5/12.
//  Copyright (c) 2012. All rights reserved.
//


#import "Vector3.h"
#import <Foundation/Foundation.h>

/*
 Declared this class since objects3D can be drawn as one entity since they have
 same material/texture properties. And we want one collection of such objects.
 
 Moreover, we will like the model to behave as one object during a 3D applications
 */
@class Object3D;

@interface Model3D : NSObject
{
    NSString *modelName;
    NSMutableArray* objects3D;
}

@property (nonatomic) NSMutableArray*  objects3D;
@property (nonatomic) NSString *modelName;
@property (nonatomic) Vector3 position;
@property (nonatomic) Vector3 scale;

-(void)addObject3D:(Object3D*)obj3D;
-(void)normalizeModel;
@end
