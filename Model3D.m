//
//  Model3D.m
//  OpenGLES13
//
//  Created by AH on 5/5/12.
//  Copyright (c) 2012. All rights reserved.
//

#import "Model3D.h"
#import "Object3D.h"

@implementation Model3D
@synthesize objects3D, modelName, position, scale;

-(id)init
{
    if (self = [super init]) 
    {
        objects3D = [[NSMutableArray alloc] init];
        modelName = @"";
    }
    return self;
}

-(void)addObject3D:(Object3D*)obj3D
{
    [objects3D addObject:obj3D];
}

-(void)normalizeModel
{
    float max = 0;
    for (Object3D *obj in objects3D)
    {
        for (int i = 0; i<obj.completeDataSize; i+=obj.stride) 
        {
            float x = fabs(obj.completeData[i]);
            float y = fabs(obj.completeData[i+1]);
            float z = fabs(obj.completeData[i+2]);
            
            if (max < MAX(x, MAX(y, z)))
                max = MAX(x, MAX(y, z));
        }
    }
    
    for (Object3D *obj in objects3D)
    {
        for (int i = 0; i < obj.completeDataSize; i += obj.stride)
        {
            obj.completeData[i] = obj.completeData[i] / max;
            obj.completeData[i+1] = obj.completeData[i+1] / max;
            obj.completeData[i+2] = obj.completeData[i+2] / max;
        }
    }
}

@end
