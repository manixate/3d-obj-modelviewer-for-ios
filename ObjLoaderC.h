//
//  ObjLoaderC.h
//  3DModelViewer
//
//  Created by Muhammad Azeem on 8/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Model3D.h"
#include <string.h>
#include <stdlib.h>
#import "MaterialStruct.h"

struct MaterialArr
{
    struct Material **materials;
    int materialIdx;
};

struct GroupInfo
{
    int numOfCompleteData;
    int numOfVertices;
    int numOfTexCoords;
    int numOfNormals;
    int numOfFaces;
    bool hasNormals;
    bool hasTexCoords;
    int completeDataStride;
};

@interface ObjLoaderC : NSObject
{
    GLfloat **vertices;
    GLfloat **texCoords;
    GLfloat **normals;
    GLfloat *completeData;
    int completeDataIdx;
    int completeDataStride;
    int hasNormals;
    int hasTexCoords;
    
    int numOfVertices;
    int numOfTexCoords;
    int numOfNormals;
    int numOfGroups;
    
    int verticesIdx;
    int texCoordsIdx;
    int normalsIdx;
    int kx;
    
    struct MaterialArr materialsArr;
    
    struct GroupInfo *groupInfos;
}

@property (nonatomic) GLfloat *completeData;
@property (nonatomic) int completeDataIdx;

- (Model3D *)loadObj:(NSString *)path;
- (void)findCaseOfFace:(char *)face;
- (void)preparseFileData:(char *)fileData;
- (int)countOccurenceOfCharacter:(const char)character inString:(const char *)string;
- (float *)parseFloatWithChar:(char *)string num:(int)num withPrefix:(char *)prefix andToken:(char *)token;
- (char **)parseStringWithChar:(char *)string num:(int)num withPrefix:(char *)prefix andToken:(char *)token;
- (int)getIdxOfMaterialWithName:(char *)name;
- (int)getTextureIdInMaterialsArr:(struct MaterialArr *)materials ofFile:(char *)fileName;
- (int *)parseIntWithChar:(char *)string num:(int)num withPrefix:(char *)prefix andToken:(char *)token;
- (float *)parseFloatWithString:(char *)string num:(int)num withPrefix:(char *)prefix andToken:(char *)token;
- (void)parseFace:(char *)string;
- (struct MaterialArr)parseMaterial:(char *)line;

@end
