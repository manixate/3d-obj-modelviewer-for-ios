//
//  ObjLoaderC.m
//  3DModelViewer
//
//  Created by Muhammad Azeem on 8/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ObjLoaderC.h"
#import "fast_atof.h"
#import "Object3D.h"
#import "GLtextureLoader.h"
#import "StringUtility.h"

#define USE_REALLOC 0

@implementation ObjLoaderC

@synthesize completeData, completeDataIdx;

- (id)init
{
    self = [super init];
    if (self) {
        completeData = NULL;
        completeDataIdx = 0;
    }
    return self;
}

- (Model3D *)loadObj:(NSString *)path
{
    NSLog(@"Loading Obj File");
    
    Model3D *model3D = [[Model3D alloc] init];
    
    NSError *err;
    NSString* objData = [NSString stringWithContentsOfFile:path encoding:NSASCIIStringEncoding error:&err];
    
    if (err)
    {
        NSLog(@"Error Reading File %@", err);
        return nil;
    }
    
//    NSMutableArray *lines = [[objData componentsSeparatedByString:@"\r\n"] mutableCopy];
//    objData = nil;
    
    verticesIdx = 0;
    texCoordsIdx = 0;
    normalsIdx = 0;
    
    Object3D *currModel = nil;
    
    char *lines = malloc(([objData length] + 1) * sizeof(char));
    [objData getCString:lines maxLength:[objData length] + 1 encoding:NSUTF8StringEncoding];
    
    objData = nil;
    
    lines = str_replace(lines, "\t", "  ");
    
    NSLog(@"Pre-Parse Started");
    [self preparseFileData:lines];
    NSLog(@"Pre-Parse Ended");
    
    char *line = strtok(lines, "\r\n");

//#if !USE_REALLOC
    vertices = malloc(numOfVertices * sizeof(GLfloat *));
    texCoords = malloc(numOfTexCoords * sizeof(GLfloat *));
    normals = malloc(numOfNormals * sizeof(GLfloat *));
//#endif
    
    kx = 0;
    
    while (line != NULL)
    {
        if (line[0] == 'v')
        {
            if (line[1] == ' ')
            {
//#if USE_REALLOC
//                vertices = realloc(vertices, (verticesIdx + 1) * sizeof(GLfloat *));
//#endif
                vertices[verticesIdx++] = [self parseFloatWithString:line num:3 withPrefix:"v" andToken:" "];
            }
            else if (line[1] == 'n')
            {
//#if USE_REALLOC
//                normals = realloc(normals, (normalsIdx + 1) * sizeof(GLfloat *));
//#endif               
                normals[normalsIdx++] = [self parseFloatWithString:line num:3 withPrefix:"vn" andToken:" "];
            }
            else if (line[1] == 't')
            {
//#if USE_REALLOC
//                texCoords = realloc(texCoords, (texCoordsIdx + 1) * sizeof(GLfloat *));
//#endif
                texCoords[texCoordsIdx++] = [self parseFloatWithString:line num:2 withPrefix:"vt" andToken:" "];
            }
        }
        else if (line[0] == 'f')
        {
//#if !USE_REALLOC
            if (hasNormals == -1 && hasTexCoords == -1)
            {
//                [self findCaseOfFace:line];
//                completeData = malloc(groupInfos[kx].numOfCompleteData * sizeof(GLfloat *));
//                hasNormals = groupInfos[kx].hasNormals;
//                hasTexCoords = groupInfos[kx].hasTexCoords;
//                completeData = malloc(tempArr[kx++] * sizeof(int *));
            }
//#endif
            [self parseFace:line];
        }
        else if ((strstr(line, "mtllib ")))
        {
            materialsArr = [self parseMaterial:line];
        }
        else if ((strstr(line, "usemtl ")))
        {            
            char **name = [self parseStringWithChar:line num:1 withPrefix:"usemtl" andToken:" "];
            char *mtlName = name[0];
            
            int idx = [self getIdxOfMaterialWithName:mtlName];
            
            free(mtlName);
            free(name);
            
            name = NULL;
            
            if (currModel.material != 0)
                continue;
            
            if (idx >= 0)
            {
                //currModel.material = materialsArr.materials[idx];
                currModel.material = malloc(sizeof(struct Material));
                currModel.material->materialName = strdup(materialsArr.materials[idx]->materialName);
                if (strcmp(materialsArr.materials[idx]->map_Ka, "") != 0)
                    currModel.material->map_Ka = strdup(materialsArr.materials[idx]->map_Ka);
                else
                    currModel.material->map_Ka = "";
                if (strcmp(materialsArr.materials[idx]->map_Kd, "") != 0)
                    currModel.material->map_Kd = strdup(materialsArr.materials[idx]->map_Kd);
                else
                    currModel.material->map_Kd = "";
                if (strcmp(materialsArr.materials[idx]->map_Ks, "") != 0)
                    currModel.material->map_Ks = strdup(materialsArr.materials[idx]->map_Ks);
                else
                    currModel.material->map_Ks = "";
                currModel.material->illum = materialsArr.materials[idx]->illum;
                currModel.material->Ns = materialsArr.materials[idx]->Ns;
                currModel.material->Tr = materialsArr.materials[idx]->Tr;
                currModel.material->hasTexture = materialsArr.materials[idx]->hasTexture;
                currModel.material->textureID = materialsArr.materials[idx]->textureID;
                
                for (int i = 0; i < 4; i++)
                {
                    currModel.material->Ka[i] = materialsArr.materials[idx]->Ka[i];
                    currModel.material->Kd[i] = materialsArr.materials[idx]->Kd[i];
                    currModel.material->Ks[i] = materialsArr.materials[idx]->Ks[i];
                }
                
                if(currModel.material!=nil && materialsArr.materials[idx]->textureID != 9999)
                {
                    currModel.material->textureID = materialsArr.materials[idx]->textureID;
//                    GLtextureLoader *texLoader = [[GLtextureLoader alloc] init];
//                    //NSLog(@"%@  %@", mat->map_Ka, mat->map_Kd);
//                    int textureID = [texLoader loadTexture:materialsArr.materials[idx]->map_Kd];
////                    if(!currModel.material->map_Ka)
////                        textureID = [GLtextureLoader loadTexture:name];
//                    currModel.textureID = textureID;
                }
            }
        }
        else if (line[0] == 'g')
        {
            hasNormals = hasTexCoords = -1;
            if (currModel == nil)
            {
                currModel = [[Object3D alloc] init];
                completeData = malloc(groupInfos[kx].numOfCompleteData * sizeof(GLfloat *));
                hasNormals = groupInfos[kx].hasNormals;
                hasTexCoords = groupInfos[kx].hasTexCoords;
//#if !USE_REALLOC
//                completeData = malloc(tempArr[kx++] * sizeof(int *));
//#endif
            }
            else if (currModel != nil)
            {
                currModel.completeData = completeData;
                currModel.completeDataSize = completeDataIdx;
                currModel.stride = groupInfos[kx].completeDataStride;
                currModel.hasNormals = groupInfos[kx].hasNormals;
                currModel.hasTexCoords = groupInfos[kx].hasTexCoords;
                
                [model3D.objects3D addObject:currModel];
                
                completeData = NULL;
                completeDataIdx = 0;
                completeDataStride = 0;
                hasNormals = -1;
                hasTexCoords = -1;
                
                currModel = nil;
                currModel = [[Object3D alloc] init];
                kx++;
                
                completeData = malloc(groupInfos[kx].numOfCompleteData * sizeof(GLfloat *));
                hasNormals = groupInfos[kx].hasNormals;
                hasTexCoords = groupInfos[kx].hasTexCoords;
//#if !USE_REALLOC
//                completeData = malloc(tempArr[kx++] * sizeof(int *));
//#endif
            }
        }
        
        line = strtok(NULL, "\r\n");
    }
    
    currModel.completeData = completeData;
    currModel.completeDataSize = completeDataIdx;
    currModel.stride = groupInfos[kx].completeDataStride;
    currModel.hasNormals = groupInfos[kx].hasNormals;
    currModel.hasTexCoords = groupInfos[kx].hasTexCoords;
    
    [model3D.objects3D addObject:currModel];
    
    completeData = NULL;
    completeDataIdx = 0;
    
    currModel = nil;
//    currModel = [[Object3D alloc] init];
    
    for (int i = 0; i < completeDataIdx - 7; i+=8)
    {
//        printf("%f, %f, %f,  %f, %f, %f,  %f, %f\n", completeData[i], completeData[i + 1], completeData[i + 2], completeData[i + 3], completeData[i + 4], completeData[i + 5], completeData[i + 6], completeData[i + 7]);
    }
    
    NSLog(@"Model Loaded");
    
//    lines = nil;
    free(groupInfos);
    for (int i = 0; i < verticesIdx; i++)
        free(vertices[i]);
    for (int i = 0; i < normalsIdx; i++)
        free(normals[i]);
    for (int i = 0; i < texCoordsIdx; i++)
        free(texCoords[i]);
    
    for (int i = 0; i < materialsArr.materialIdx; i++)
    {
        if (strcmp(materialsArr.materials[i]->materialName, "") != 0)
            free(materialsArr.materials[i]->materialName);
        if (strcmp(materialsArr.materials[i]->map_Kd, "") != 0)
            free(materialsArr.materials[i]->map_Kd);
        if (strcmp(materialsArr.materials[i]->map_Ka, "") != 0)
            free(materialsArr.materials[i]->map_Ka);
        if (strcmp(materialsArr.materials[i]->map_Ks, "") != 0)
            free(materialsArr.materials[i]->map_Ks);
        
        free(materialsArr.materials[i]);
    }

    free(vertices);
    free(normals);
    free(texCoords);
    
    free(materialsArr.materials);
    
    free(lines);
    
    return model3D;
}

- (void)findCaseOfFace:(char *)face
{
    char *result = NULL;
    result = strtok_r(face, " ", &face);
    
    result = strtok_r(NULL, " ", &face);
    int countOfSlashes = [self countOccurenceOfCharacter:'/' inString:result];
    
    if (countOfSlashes == 0)
    {
        hasNormals = 0;
        hasTexCoords = 0;
        
        completeDataStride = 3;
    }
    else if (countOfSlashes == 1)
    {
        hasNormals = 0;
        hasTexCoords = 1;
        
        completeDataStride = 5;
    }
    else if (countOfSlashes == 2)
    {
        if (strstr(result, "//"))
        {
            hasNormals = 1;
            hasTexCoords = 0;
            
            completeDataStride = 6;
        }
        else
        {
            hasNormals = 1;
            hasTexCoords = 1;
            
            completeDataStride = 8;
        }
    }
}

- (int)countOccurenceOfCharacter:(const char)character inString:(const char *)string
{
    int count = 0;
    for (int i = 0; i < strlen(string); i++)
    {
        if (character == string[i])
            count++;
    }
    
    return count;
}

- (void)preparseFileData:(char *)fileData
{
    char *dataCopy = strdup(fileData);
    
    char *line = NULL;
    line = strtok(dataCopy, "\r\n");
    
    groupInfos = NULL;
    struct GroupInfo currGroupInfo = {0, 0, 0, 0, -1, -1};
    
    hasNormals = -1;
    hasTexCoords = -1;
    
    while (line != NULL)
    {
        if (line[0] == 'v')
        {
            if (line[1] == ' ')
            {
                numOfVertices++;
            }
            else if (line[1] == 'n')
            {
                numOfNormals++;
            }
            else if (line[1] == 't')
            {
                numOfTexCoords++;
            }
        }
        else if (line[0] == 'f')
        {
            currGroupInfo.numOfFaces++;
            
            if (hasNormals == -1 && hasTexCoords == -1)
            {
                [self findCaseOfFace:line];
                currGroupInfo.hasNormals = hasNormals;
                currGroupInfo.hasTexCoords = hasTexCoords;
            }
            
            if (hasNormals == 1 && hasTexCoords == 1)
            {
                currGroupInfo.completeDataStride = 8;
                currGroupInfo.numOfCompleteData += (currGroupInfo.completeDataStride * 3);
            }
            else if (hasNormals == 1)
            {
                currGroupInfo.completeDataStride = 6;
                currGroupInfo.numOfCompleteData += (currGroupInfo.completeDataStride * 3);
            }
            else if (hasTexCoords == 1)
            {
                currGroupInfo.completeDataStride = 5;
                currGroupInfo.numOfCompleteData += (currGroupInfo.completeDataStride * 3);
            }
            else
            {
                currGroupInfo.completeDataStride = 3;
                currGroupInfo.numOfCompleteData += (currGroupInfo.completeDataStride * 3);
            }
        }
        else if (line[0] == 'g')
        {
            if (groupInfos == NULL)
                groupInfos = malloc(sizeof(struct GroupInfo));
            else
            {
                groupInfos[numOfGroups++] = currGroupInfo;
                
                currGroupInfo.numOfVertices = 0;
                currGroupInfo.numOfNormals = 0;
                currGroupInfo.numOfTexCoords = 0;
                currGroupInfo.numOfCompleteData = 0;
                currGroupInfo.hasNormals = -1;
                currGroupInfo.hasTexCoords = -1;
                currGroupInfo.numOfFaces = 0;
                
                hasNormals = -1;
                hasTexCoords = -1;
                
                groupInfos = realloc(groupInfos, (numOfGroups + 1) * sizeof(struct GroupInfo));
            }
        }
        
        line = strtok(NULL, "\r\n");
    }
    
    if (groupInfos != NULL)
        groupInfos[numOfGroups++] = currGroupInfo;
    
    currGroupInfo.numOfVertices = 0;
    currGroupInfo.numOfNormals = 0;
    currGroupInfo.numOfTexCoords = 0;
    currGroupInfo.numOfCompleteData = 0;
    currGroupInfo.hasNormals = -1;
    currGroupInfo.hasTexCoords = -1;
    currGroupInfo.numOfFaces = 0;
    
    hasNormals = -1;
    hasTexCoords = -1;
    
    free(dataCopy);
}

- (struct MaterialArr)parseMaterial:(char *)line
{
    char *data = NULL;
    
    char *lineCopy = strdup(line);
    lineCopy = str_replace(lineCopy, "\t", "");
    data = lineCopy;
    char *mtl = strtok_r(lineCopy, " ", &lineCopy);
    mtl = strtok_r(NULL, " ", &lineCopy);
    
    NSString *mtlFileName = [[NSString stringWithCString:mtl encoding:NSUTF8StringEncoding] stringByDeletingPathExtension];
    
    NSString *objData = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:mtlFileName ofType:@"mtl"] encoding:NSUTF8StringEncoding error:nil];
    
    free(data);
    
    char *lines = malloc(([objData length] + 1) * sizeof(char));
    [objData getCString:lines maxLength:[objData length] + 1 encoding:NSUTF8StringEncoding];
    
    objData = nil;
    
    lines = str_replace(lines, "\t", "");
    data = lines;
    
    char *result = strtok_r(lines, "\r\n", &lines);
    
    struct Material *material = NULL;
    
    struct MaterialArr materials;
    materials.materialIdx = 0;
    materials.materials = malloc(sizeof(struct Material*));
    
    GLtextureLoader *texLoader = [[GLtextureLoader alloc] init];
    
    while (result != NULL)
    {
        if ((strstr(result, "newmtl ")))
        {
            if (material == NULL)
            {
                material = malloc(sizeof(struct Material));
                material->materialName = "";
                material->map_Kd = "";
                material->map_Ka = "";
                material->map_Ks = "";
                
                char **values = [self parseStringWithChar:result num:1 withPrefix:"newmtl" andToken:" "];
                
                material->materialName = values[0];
                
//                for (int i = 0; i < 1; i++)
//                    free(values[i]);
                free(values);
            }
            else
            {
                if (material->hasTexture)
                {
                    int idx = [self getTextureIdInMaterialsArr:&materials ofFile:material->map_Kd];
                    int textureID;
                    if (idx >= 0)
                        textureID = materials.materials[idx]->textureID;
                    else
                        textureID = [texLoader loadTexture:material->map_Kd];
                    
                    //NSLog(@"%@  %@", mat->map_Ka, mat->map_Kd);
                    if(!material->map_Kd)
                    {
                        idx = [self getTextureIdInMaterialsArr:&materials ofFile:material->map_Ka];
                        
                        if (idx >= 0)
                            textureID = materials.materials[idx]->textureID;
                        else
                            textureID = [texLoader loadTexture:material->map_Ka];
                    }
                    material->textureID = textureID;
                }
                
                materials.materials[materials.materialIdx++] = material;
                materials.materials = realloc(materials.materials, (materials.materialIdx + 1) * sizeof(struct Material));
                
                material = malloc(sizeof(struct Material));
                material->materialName = "";
                material->map_Kd = "";
                material->map_Ka = "";
                material->map_Ks = "";
                
                char **values = [self parseStringWithChar:result num:1 withPrefix:"newmtl" andToken:" "];
                
                material->materialName = values[0];
                
                free(values);
            }
            
            material->hasTexture = false;
        }
        else if ((strstr(result, "map_Kd ")))
        {
            material->hasTexture = true;
            
            char **values = [self parseStringWithChar:result num:1 withPrefix:"map_Kd" andToken:" "];
            
            material->map_Kd = values[0];
            
            free(values);
        }
        else if ((strstr(result, "map_Ka ")))
        {
            material->hasTexture = true;
            
            char **values = [self parseStringWithChar:result num:1 withPrefix:"map_Ka" andToken:" "];
            
            material->map_Ka = values[0];
            
            free(values);
        }
        else if ((strstr(result, "map_Ks ")))
        {
            material->hasTexture = true;
            
            char **values = [self parseStringWithChar:result num:1 withPrefix:"map_Ks" andToken:" "];
            
            material->map_Ks = values[0];
            
            free(values);
        }
        else if ((strstr(result, "Ka ")))
        {
            float *values = [self parseFloatWithChar:result num:3 withPrefix:"Ka" andToken:" "];
            
            material->Ka[0] = values[0];
            material->Ka[1] = values[1];
            material->Ka[2] = values[2];
            material->Ka[3] = 1.0;
            
            free(values);
        }
        else if ((strstr(result, "Ks ")))
        {
            float *values = [self parseFloatWithChar:result num:3 withPrefix:"Ks" andToken:" "];
            
            material->Ks[0] = values[0];
            material->Ks[1] = values[1];
            material->Ks[2] = values[2];
            material->Ks[3] = 1.0;
            
            free(values);
        }
        else if ((strstr(result, "Kd ")))
        {
            float *values = [self parseFloatWithChar:result num:3 withPrefix:"Kd" andToken:" "];
            
            material->Kd[0] = values[0];
            material->Kd[1] = values[1];
            material->Kd[2] = values[2];
            material->Kd[3] = 1.0;
            
            free(values);
        }
        else if ((strstr(result, "illum ")))
        {
            int *values = [self parseIntWithChar:result num:1 withPrefix:"illum" andToken:" "];
            
            material->illum = values[0];
            
            free(values);
        }
        else if ((strstr(result, "Ns ")))
        {
            float *values = [self parseFloatWithChar:result num:1 withPrefix:"Ns" andToken:" "];
            
            material->Ns = values[0];
            
            free(values);
        }
        else if ((strstr(result, "Tr ")))
        {
            float *values = [self parseFloatWithChar:result num:1 withPrefix:"Tr" andToken:" "];
            
            material->Tr = values[0];
            
            free(values);
        }
        
        result = strtok_r(NULL, "\r\n", &lines);
    }
    
    if (material->hasTexture)
    {        
        int idx = [self getTextureIdInMaterialsArr:&materials ofFile:material->map_Kd];
        int textureID;
        if (idx >= 0)
            textureID = materials.materials[idx]->textureID;
        else
            textureID = [texLoader loadTexture:material->map_Kd];
        
        //NSLog(@"%@  %@", mat->map_Ka, mat->map_Kd);
        if(!material->map_Kd)
        {
            idx = [self getTextureIdInMaterialsArr:&materials ofFile:material->map_Ka];
            
            if (idx >= 0)
                textureID = materials.materials[idx]->textureID;
            else
                textureID = [texLoader loadTexture:material->map_Ka];
        }
        material->textureID = textureID;
    }
    
    materials.materials[materials.materialIdx++] = material;
    
    texLoader = nil;
    
    free(data);
    
    return materials;
}

- (int)getIdxOfMaterialWithName:(char *)name
{
    int idx = -1;
    
    for (int i = 0; i < materialsArr.materialIdx; i++)
    {
        if (strcmp(materialsArr.materials[i]->materialName, name) == 0)
        {
            idx = i;
            return idx;
        }
    }
    
    return idx;
}

- (int)getTextureIdInMaterialsArr:(struct MaterialArr *)materials ofFile:(char *)fileName
{
    int idx = -1;
    
    for (int i = 0; i < materials->materialIdx; i++)
    {
        if (strcmp(materials->materials[i]->map_Kd, fileName) == 0)
        {
            idx = i;
            return idx;
        }
        if (strcmp(materials->materials[i]->map_Ka, fileName) == 0)
        {
            idx = i;
            return idx;
        }
        if (strcmp(materials->materials[i]->map_Ks, fileName) == 0)
        {
            idx = i;
            return idx;
        }
    }
    
    return idx;
}

- (char **)parseStringWithChar:(char *)string num:(int)num withPrefix:(char *)prefix andToken:(char *)token
{
    char **strings = NULL;
    
    int i = 0;
    char *result = NULL;
    result = strtok_r(string, token, &string);
    strings = malloc(num * sizeof(char *));
    while( result != NULL )
    {
        if (strcmp(result, prefix) != 0)
        {
//            strings = realloc(strings, (i + 1) * sizeof(char *));
            if (i < num)
            {
                strings[i++] = strdup(result);
            }
        }
        
        //        printf( "result is \"%s\"\n", result );
        result = strtok_r( NULL, token, &string );
    }
    
    return strings;
}

- (float *)parseFloatWithString:(char *)string num:(int)num withPrefix:(char *)prefix andToken:(char *)token
{
//    char *buf = malloc(([string length] + 1) * sizeof(char));
//    [string getCString:buf maxLength:[string length] + 1 encoding:NSUTF8StringEncoding];
    
    float *numbers = NULL;
    
    int i = 0;
    char *result = NULL;
    result = strtok_r(string, token, &string);
    
    numbers = malloc(num * sizeof(float));
    
    while( result != NULL )
    {
        if (strcmp(result, prefix) != 0)
        {
//            numbers = realloc(numbers, (i + 1) * sizeof(float));
            if (i < num)
            {
                numbers[i++] = atof(result);
            }
        }
        
//        printf( "result is \"%s\"\n", result );
        result = strtok_r( NULL, token, &string );
    }
    
    return numbers;
}

- (float *)parseFloatWithChar:(char *)string num:(int)num withPrefix:(char *)prefix andToken:(char *)token
{    
    float *numbers = NULL;
    
    int i = 0;
    char *result = NULL;
    result = strtok_r(string, token, &string);
    
    numbers = malloc(num * sizeof(float));
    
    while( result != NULL )
    {
        if (strcmp(result, prefix) != 0)
        {
//            numbers = realloc(numbers, (i + 1) * sizeof(float));
            if (i < num)
            {
                numbers[i++] = atof(result);
            }
        }
        
        //        printf( "result is \"%s\"\n", result );
        result = strtok_r(NULL, token, &string);
    }
    
    return numbers;
}

- (int *)parseIntWithChar:(char *)string num:(int)num withPrefix:(char *)prefix andToken:(char *)token
{    
    int *numbers = NULL;
    
    int i = 0;
    char *result = NULL;
    result = strtok_r(string, token, &string);
    
    numbers = malloc(num * sizeof(int));
    
    while( result != NULL )
    {
        if (strcmp(result, prefix) != 0)
        {
//            numbers = realloc(numbers, (i + 1) * sizeof(int));
            if (i < num)
            {
                numbers[i++] = atoi(result);
            }
        }
        
        //        printf( "result is \"%s\"\n", result );
        result = strtok_r( NULL, token, &string );
    }
    
    return numbers;
}

- (void)parseFace:(char *)string
{
//    char *buf = malloc(([string length] + 1) * sizeof(char));
//    [string getCString:buf maxLength:[string length] + 1 encoding:NSUTF8StringEncoding];
    
    char *result = NULL;
    result = strtok_r(string, " ", &string);
    int n = 0;
    
    if (hasNormals == 1 && hasTexCoords == 1)
        n = 3;
    else if (hasNormals == 0 && hasTexCoords == 0)
        n = 1;
    else
        n = 2;
    
    while( result != NULL )
    {
        //        printf( "result is \"%s\"\n", result );
        if (strcmp(result, "f") != 0 && strcmp(result, "\r") != 0)
        {
            int *vertex;
            //            char *resBuf = malloc(strlen(result));
            //            strcpy(resBuf, result);
            vertex = [self parseIntWithChar:result num:n withPrefix:"" andToken:"/"];
            
            //            printf("Vertex: %i, %i, %i\n", vertex[0], vertex[1], vertex[2]);
            //#if USE_REALLOC
            //            completeData = realloc(completeData, 8 * (completeDataIdx + 1) * sizeof(int *));
            //#endif
            completeData[completeDataIdx++] = vertices[vertex[0] - 1][0];
            completeData[completeDataIdx++] = vertices[vertex[0] - 1][1];
            completeData[completeDataIdx++] = vertices[vertex[0] - 1][2];
            if (hasNormals == 1 && hasTexCoords == 1)
            {                
                completeData[completeDataIdx++] = texCoords[vertex[1] - 1][0];
                completeData[completeDataIdx++] = 1 - texCoords[vertex[1] - 1][1];
                
                completeData[completeDataIdx++] = normals[vertex[2] - 1][0];
                completeData[completeDataIdx++] = normals[vertex[2] - 1][1];
                completeData[completeDataIdx++] = normals[vertex[2] - 1][2];
            }
            else
            {
                if (hasTexCoords == 1)
                {
                    completeData[completeDataIdx++] = texCoords[vertex[1] - 1][0];
                    completeData[completeDataIdx++] = 1 - texCoords[vertex[1] - 1][1];
                }
                else if (hasNormals == 1)
                {
                    completeData[completeDataIdx++] = normals[vertex[1] - 1][0];
                    completeData[completeDataIdx++] = normals[vertex[1] - 1][1];
                    completeData[completeDataIdx++] = normals[vertex[1] - 1][2];
                }
            }
            
            free(vertex);
        }
        result = strtok_r( NULL, " ", &string );
    }
}

@end