//
//  DrawModel.h
//  OpenGLES13
//
//  Created by AH on 5/5/12.
//  Copyright (c) 2012. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import "MaterialStruct.h"

@interface Object3D : NSObject

@property (nonatomic) struct Material *material;
@property (nonatomic) GLfloat*        completeData;
@property (nonatomic) int             completeDataSize;
@property (nonatomic) int             stride;
@property (nonatomic) bool            hasNormals;
@property (nonatomic) bool            hasTexCoords;

@end