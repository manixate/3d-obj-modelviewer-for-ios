//
//  DrawModel.m
//  OpenGLES13
//
//  Created by AH on 5/5/12.
//  Copyright (c) 2012. All rights reserved.
//

#import "GLtextureLoader.h"
#import "Object3D.h"

@implementation Object3D
@synthesize completeData, completeDataSize, stride;
@synthesize hasNormals, hasTexCoords;

@synthesize material;

- (id)init
{
    self = [super init];
    if (self) {
        completeData = 0;
        completeDataSize = 0;
        stride = 0;
        hasNormals = false;
        hasTexCoords = false;
        material = 0;
    }
    return self;
}

- (void)dealloc
{
    free(completeData);
    
    if (material)
    {
        free(material->materialName);
        if (material->map_Kd && strcmp(material->map_Kd, "") != 0)
            free(material->map_Kd);
        if (material->map_Ka && strcmp(material->map_Ka, "") != 0)
            free(material->map_Ka);
        if (material->map_Ks && strcmp(material->map_Ks, "") != 0)
            free(material->map_Ks);
        
        glDeleteTextures(1, &material->textureID);
        
        free(material);
    }
}

@end



