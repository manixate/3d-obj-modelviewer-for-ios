//
//  RenderingEngine.h
//  ObjModelViewer
//
//  Created by Muhammad Azeem on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MathUtility.h"
#import "Model3D.h"
#import "Scene.h"
#import "ObjLoaderC.h"
#import "Matrix4.h"
#import <QuartzCore/QuartzCore.h>

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

// Uniform index.
enum
{
    UNIFORM_MODELVIEWPROJECTION_MATRIX,
    UNIFORM_NORMAL_MATRIX,
    UNIFORM_TEXTURE,
    UNIFORM_AMBIENTCOLOR,
    UNIFORM_DIFFUSECOLOR,
    UNIFORM_SPECULARCOLOR,
    UNIFORM_HASTEXTURE,
    NUM_UNIFORMS
};
GLint uniforms[NUM_UNIFORMS];

// Attribute index.
enum
{
    ATTRIB_VERTEX,
    ATTRIB_NORMAL,
    ATTRIB_TEXTURE_COORD,
    NUM_ATTRIBUTES
};

@interface RenderingEngine : NSObject
{
    GLuint mFrameBuffer;
    GLuint mColorRenderBuffer;
    GLuint mDepthBuffer;
    
    GLuint mSampleFrameBuffer;
    GLuint mSampleColorRenderBuffer;
    GLuint mSampleDepthBuffer;
    
    bool isMSAAEnabled;
    
    int framebufferWidth, framebufferHeight;
    
    GLfloat scaleFactor;
    GLfloat xRotation, yRotation;
    GLfloat xTranslation, yTranslation, zTranslation;
    
    Scene *scene3D;
    
    GLuint **_vertexBuffer;
    
    CATransform3D transform;
    
    bool isFirstDrawing;
    
    GLuint _skyboxBuffer;
    GLuint _skyboxIndicesBuffer;
}

@property (nonatomic, retain) Scene *scene3D;
@property (nonatomic) NSString *modelName;
@property (nonatomic) GLfloat scaleFactor;
@property (nonatomic) GLfloat xRotation, yRotation;
@property (nonatomic) GLfloat xTranslation, yTranslation, zTranslation;

- (id) init;
- (id) initWithModelNames:(NSArray *)modelNames;
- (id) initWithModelNames:(NSArray *)modelNames positions:(Vector3 *)positions scales:(Vector3 *)scales;
- (void) initFrameBuffer;
- (void) update:(NSTimeInterval)aTimeStamp;
- (void) updateWithScale:(GLfloat)scaleFactor rotation:(CGPoint)rotation translation:(Vector3)translation andTimeStamp:(NSTimeInterval)aTimeStamp;
- (void) render;
- (void) onTouchBegin:(CGPoint)point andNumOfTouches:(int)numOfTouches;
- (void) onTouchMovedFromPoint:(CGPoint)prevPoint toPoint:(CGPoint)point andNumOfTouches:(int)numOfTouches;
- (void) onTouchEnd:(CGPoint)point andNumOfTouches:(int)numOfTouches;

- (void)toggleMSAA;
- (void)setMSAA:(BOOL)flag;

@end
