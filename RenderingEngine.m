//
//  RenderingEngine.m
//  ObjModelViewer
//
//  Created by Muhammad Azeem on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RenderingEngine.h"
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import "Object3D.h"

@implementation RenderingEngine
@synthesize modelName;

@synthesize scene3D;
@synthesize scaleFactor, xRotation, yRotation, xTranslation, yTranslation, zTranslation;

- (id)init
{
    self = [super init];
    if (self)
    {
        isMSAAEnabled = true;
        //        NSString *path = [[NSBundle mainBundle] pathForResource:@"table" ofType:@"obj"];
        //        ObjLoader *theObject = [[ObjLoader alloc] initWithPath:path];
        //        
        //        Model3D *model3D = [theObject copyObj2Model3D];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"table" ofType:@"obj"];
        ObjLoaderC *objLoader = [[ObjLoaderC alloc]init];
        
        Model3D *model3D = [objLoader loadObj:path];
        
        [model3D normalizeModel];
        
        scene3D = [[Scene alloc] init];
        [scene3D.models addObject:model3D];
        
        // Generate ONE Color Render Buffer and bind the default Render Buffer to this Color Render Buffer
        glGenRenderbuffersOES(1, &mColorRenderBuffer);
        glBindRenderbufferOES(GL_RENDERBUFFER_OES, mColorRenderBuffer);
        
        _vertexBuffer = malloc(scene3D.models.count * sizeof(GLuint*));
        for (int i = 0; i < scene3D.models.count; i++)
        {
            Model3D *model = [scene3D.models objectAtIndex:i];
            _vertexBuffer[i] = malloc(model.objects3D.count * sizeof(GLuint));
            
            glGenBuffers(model.objects3D.count, _vertexBuffer[i]);
        }
        
        for (int i = 0; i < scene3D.models.count; i++)
        {
            Model3D *model = [scene3D.models objectAtIndex:i];
            for (int j = 0; j < model.objects3D.count; j++)
            {
                Object3D *object3D = [model.objects3D objectAtIndex:j];
                glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer[i][j]);
                glBufferData(GL_ARRAY_BUFFER, object3D.completeDataSize * sizeof(GLfloat), object3D.completeData, GL_STATIC_DRAW);
            }
        }
        
    }
    
    return self;
}

- (id) initWithModelNames:(NSArray *)modelNames
{
    self = [super init];
    if (self)
    {
        isMSAAEnabled = true;
        
        scene3D = [[Scene alloc] initWithModelNames:modelNames];
        
        glGenRenderbuffersOES(1, &mColorRenderBuffer);
        glBindRenderbufferOES(GL_RENDERBUFFER_OES, mColorRenderBuffer);
        
        _vertexBuffer = malloc(scene3D.models.count * sizeof(GLuint*));
        for (int i = 0; i < scene3D.models.count; i++)
        {
            Model3D *model = [scene3D.models objectAtIndex:i];
            _vertexBuffer[i] = malloc(model.objects3D.count * sizeof(GLuint));
            
            glGenBuffers(model.objects3D.count, _vertexBuffer[i]);
        }
        
        for (int i = 0; i < scene3D.models.count; i++)
        {
            Model3D *model = [scene3D.models objectAtIndex:i];
            for (int j = 0; j < model.objects3D.count; j++)
            {
                Object3D *object3D = [model.objects3D objectAtIndex:j];
                glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer[i][j]);
                glBufferData(GL_ARRAY_BUFFER, object3D.completeDataSize * sizeof(GLfloat), object3D.completeData, GL_STATIC_DRAW);
            }
        }
    }
    
    return self;
}

- (id) initWithModelNames:(NSArray *)modelNames positions:(Vector3 *)positions scales:(Vector3 *)scales
{
    self = [super init];
    if (self)
    {
        isMSAAEnabled = true;
        
        scene3D = [[Scene alloc] initWithModelNames:modelNames positions:positions andScale:scales];
        
        glGenRenderbuffersOES(1, &mColorRenderBuffer);
        glBindRenderbufferOES(GL_RENDERBUFFER_OES, mColorRenderBuffer);
        
        _vertexBuffer = malloc(scene3D.models.count * sizeof(GLuint*));
        for (int i = 0; i < scene3D.models.count; i++)
        {
            Model3D *model = [scene3D.models objectAtIndex:i];
            _vertexBuffer[i] = malloc(model.objects3D.count * sizeof(GLuint));
            
            glGenBuffers(model.objects3D.count, _vertexBuffer[i]);
        }
        
        for (int i = 0; i < scene3D.models.count; i++)
        {
            Model3D *model = [scene3D.models objectAtIndex:i];
            for (int j = 0; j < model.objects3D.count; j++)
            {
                Object3D *object3D = [model.objects3D objectAtIndex:j];
                glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer[i][j]);
                glBufferData(GL_ARRAY_BUFFER, object3D.completeDataSize * sizeof(GLfloat), object3D.completeData, GL_STATIC_DRAW);
            }
        }
        
        // Skybox
        float vertices[24] = {  
            -1.0, -1.0,  1.0,
            1.0, -1.0,  1.0,
            -1.0,  1.0,  1.0,
            1.0,  1.0,  1.0,
            -1.0, -1.0, -1.0,
            1.0, -1.0, -1.0,
            -1.0,  1.0, -1.0,
            1.0,  1.0, -1.0,
        };
        
        glGenBuffers(1, &_skyboxBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, _skyboxBuffer);
        
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
        
        glGenBuffers(1, &_skyboxIndicesBuffer);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _skyboxIndicesBuffer);
        
        GLubyte indices[14] = {0, 1, 2, 3, 7, 1, 5, 4, 7, 6, 2, 4, 0, 1};
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    }
    
    return self;
}

- (void)initFrameBuffer
{
    // Generate framebuffer
    glGenFramebuffersOES(1, &mFrameBuffer);
    glBindFramebufferOES(GL_FRAMEBUFFER_OES, mFrameBuffer);
    
    // Attach colorRenderBuffer with framebuffer
    glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, mColorRenderBuffer);
    
    // Generate depthBuffer and attach it with frame buffer
    int backingWidth, backingHeight;
    
    glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_WIDTH_OES, &backingWidth);
    glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_HEIGHT_OES, &backingHeight);
    
    glGenRenderbuffersOES(1, &mDepthBuffer);
    glBindRenderbufferOES(GL_RENDERBUFFER_OES, mDepthBuffer);
    glRenderbufferStorageOES(GL_RENDERBUFFER_OES, GL_DEPTH_COMPONENT16_OES, backingWidth, backingHeight);
    glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, mDepthBuffer);
    
    if(glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES) != GL_FRAMEBUFFER_COMPLETE_OES) {
        NSLog(@"failed to make complete framebuffer object %x", glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES));
    }
    
    framebufferWidth = backingWidth;
    framebufferHeight = backingHeight;
    
    if (isMSAAEnabled == true)
    {
        glGenFramebuffers(1, &mSampleFrameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, mSampleFrameBuffer);
        
        glGenRenderbuffers(1, &mSampleColorRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, mSampleColorRenderBuffer);
        glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, 4, GL_RGBA8_OES, framebufferWidth, framebufferHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, mSampleColorRenderBuffer);
        
        glGenRenderbuffers(1, &mSampleDepthBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, mSampleDepthBuffer);
        glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, 4, GL_DEPTH_COMPONENT16, framebufferWidth, framebufferHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, mSampleDepthBuffer);
        
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            NSLog(@"Failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    // Set color render buffer to be the default render buffer
    glBindRenderbufferOES(GL_RENDERBUFFER_OES, mColorRenderBuffer);
}

- (void)update:(NSTimeInterval)aTimeStamp
{
    
}

- (void)toggleMSAA
{    
    isMSAAEnabled = !isMSAAEnabled;
    
    if (isMSAAEnabled)
        NSLog(@"MSAA Enabled");
    else        
        NSLog(@"MSAA Disabled");
}

- (void)render
{
    if (isMSAAEnabled)
        glBindFramebuffer(GL_FRAMEBUFFER, mSampleFrameBuffer);
    glViewport(0, 0, framebufferWidth, framebufferHeight);
    
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    //	glMatrixMode(GL_MODELVIEW);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
}
-(void)dealloc
{
    glDeleteFramebuffersOES(1, &mFrameBuffer);
    glDeleteRenderbuffersOES(1, &mColorRenderBuffer);
    glDeleteRenderbuffersOES(1, &mDepthBuffer);
    
    glDeleteFramebuffersOES(1, &mSampleFrameBuffer);
    glDeleteRenderbuffersOES(1, &mSampleColorRenderBuffer);
    glDeleteRenderbuffersOES(1, &mSampleDepthBuffer);
    
    scene3D = nil;
}

@end
