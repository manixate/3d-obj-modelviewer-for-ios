//
//  RenderingEngineES1.h
//  ObjModelViewer
//
//  Created by Muhammad Azeem on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RenderingEngine.h"

@interface RenderingEngineES1 : RenderingEngine

- (void) setClipping;

@end
