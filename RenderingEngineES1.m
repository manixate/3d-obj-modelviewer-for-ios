//
//  RenderingEngineES1.m
//  ObjModelViewer
//
//  Created by Muhammad Azeem on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RenderingEngineES1.h"
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import "Object3D.h"
#import "ColorUtility.h"

@implementation RenderingEngineES1

- (id) init
{
    self = [super init];
    if (self)
    {        
        [self initLighting];
        [self setClipping];
    }
    
    return self;
}

- (id) initWithModelNames:(NSArray *)modelNames
{
    self = [super initWithModelNames:modelNames];
    if (self)
    {
        [self initLighting];
        [self setClipping];        
    }
    return self;
}

- (id) initWithModelNames:(NSArray *)modelNames positions:(Vector3 *)positions scales:(Vector3 *)scales
{
    self = [super initWithModelNames:modelNames positions:positions scales:scales];
    if (self)
    {
        [self initLighting];
        [self setClipping];        
    }
    return self;
}

#pragma mark - Own Methods

- (void)initFrameBuffer
{
    [super initFrameBuffer];
}

-(void)initLighting
{
	//lights go here
	
	glLightfv(GL_LIGHT0,GL_POSITION,sunPos);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,white);
	glLightfv(GL_LIGHT0,GL_SPECULAR,white);
    glLightfv(GL_LIGHT0, GL_AMBIENT,white);
	
    //	glLightfv(GL_LIGHT1,GL_POSITION,posFill1);
    //	glLightfv(GL_LIGHT1,GL_DIFFUSE,dimblue);
    //	glLightfv(GL_LIGHT1,GL_SPECULAR,dimcyan);	
    
    //	glLightfv(GL_LIGHT2,GL_POSITION,posFill2);
    //	glLightfv(GL_LIGHT2,GL_SPECULAR,dimmagenta);
    //	glLightfv(GL_LIGHT2,GL_DIFFUSE,dimblue);
    
	//materials go here
	
    //	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, cyan);
    //	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, white);
    
    //	glLightf(GL_LIGHT0,GL_QUADRATIC_ATTENUATION,.01);
	
    //	glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,25);				
    //    
	glShadeModel(GL_SMOOTH);				
    //    glLightModelf(GL_LIGHT_MODEL_TWO_SIDE,0.0);
	
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
    //	glEnable(GL_LIGHT1);
    //	glEnable(GL_LIGHT2);
}

-(void)setClipping
{
	float aspectRatio;
	const float zNear = .1;					
	const float zFar = 10000;					
	const float fieldOfView = 65.0;			
	GLfloat	size;
	
	CGRect frame = [[UIScreen mainScreen] bounds];		
    
	aspectRatio=(float)frame.size.width/(float)frame.size.height;					
	
	glMatrixMode(GL_PROJECTION);				
	glLoadIdentity();
    
	size = zNear * tanf(degreesToRadians(fieldOfView) / 2.0);	
    
	glFrustumf(-size, size, -size /aspectRatio, size /aspectRatio, zNear, zFar);
	
	glMatrixMode(GL_MODELVIEW);
}

#pragma mark - Render Method

- (void)render
{
    [super render];
    
    glMatrixMode(GL_MODELVIEW);
    
    // Draw Scene
    
    glEnable(GL_DEPTH_TEST);
    for (int i = 0; i < scene3D.models.count; i++)
    {
        static GLfloat angle = 0.0;
        GLfloat orbitalIncrement = 1.25;					
        
        //        glPushMatrix();
        
        //        glTranslatef(0, 0, -3.0);
        
        angle += orbitalIncrement;
        
        //        glRotatef(angle, 1.0, 1.0, 1.0);
        
        Model3D *model3D = [scene3D.models objectAtIndex:i];
        
        glPushMatrix();
        
        glTranslatef(model3D.position.x, model3D.position.y, model3D.position.z);
        glScalef(model3D.scale.x, model3D.scale.y, model3D.scale.z);
        
        for (int j = 0; j < model3D.objects3D.count; j++)
        {
            Object3D *object3D = [model3D.objects3D objectAtIndex:j];
            
            if (object3D.material)
            {
                glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, object3D.material->Ka);
                glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, object3D.material->Kd);
                glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, object3D.material->Ks);
            }
            
            glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer[i][j]);
            
            int offset = 0;
            glEnableClientState(GL_VERTEX_ARRAY);
            glVertexPointer(3, GL_FLOAT, object3D.stride * sizeof(GLfloat), BUFFER_OFFSET(offset));
            
            if (object3D.hasTexCoords && object3D.material)
            {
                offset += 12;
                glEnableClientState(GL_TEXTURE_COORD_ARRAY);
                
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, object3D.material->textureID);
                
                glTexCoordPointer(2, GL_FLOAT, object3D.stride * sizeof(GLfloat), BUFFER_OFFSET(offset));
            }
            
            if (object3D.hasNormals)
            {
                offset += 8;
                glEnableClientState(GL_NORMAL_ARRAY);
                glNormalPointer(GL_FLOAT, object3D.stride * sizeof(GLfloat), BUFFER_OFFSET(offset));
            }
            
            glDrawArrays(GL_TRIANGLES, 0, object3D.completeDataSize / object3D.stride);
            
            glDisableClientState(GL_VERTEX_ARRAY);
            glDisableClientState(GL_NORMAL_ARRAY);
            glDisableClientState(GL_TEXTURE_COORD_ARRAY);
            
            glDisable(GL_TEXTURE_2D);
        }
        glPopMatrix();
    }
    
    if (isMSAAEnabled)
    {
        glBindFramebufferOES(GL_DRAW_FRAMEBUFFER_APPLE, mFrameBuffer);
        glBindFramebufferOES(GL_READ_FRAMEBUFFER_APPLE, mSampleFrameBuffer);
        glResolveMultisampleFramebufferAPPLE();
    }
    else
    {
        glBindFramebufferOES(GL_FRAMEBUFFER_OES, mFrameBuffer);
    }
}

- (void)update:(NSTimeInterval)aTimeStamp
{
    [super update:aTimeStamp];
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glTranslatef(0, 0, -3);
    glTranslatef(xTranslation * M_PI / 180, yTranslation * M_PI / 180, zTranslation * M_PI / 180);
    
    float totalRotation = sqrtf(xRotation * xRotation + yRotation * yRotation);
    
    if (totalRotation != 0)
        glRotatef(totalRotation, xRotation / totalRotation, yRotation / totalRotation, 0);
    
    if (scaleFactor != 0)
        glScalef(scaleFactor, scaleFactor, scaleFactor);
    
    //    for (Model3D *model in scene3D.models)
    //    {
    //        if ([model.modelName isEqualToString:@"Chair"]) 
    //        {
    //            model.position = Vector3Make(model.position.x + 0.01, model.position.y, model.position.z);
    //        }
    //    }
}


#pragma mark - Touch Event Handlers

- (void)onTouchBegin:(CGPoint)point
{
    CATransform3DTranslate(transform, point.x, point.y, 1.0);
}

- (void)onTouchMovedFromPoint:(CGPoint)prevPoint toPoint:(CGPoint)point
{
    
}

- (void)onTouchEnd:(CGPoint)point
{
    
}

@end