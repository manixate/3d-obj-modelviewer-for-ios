//
//  RenderingEngineES2.h
//  ObjModelViewer
//
//  Created by Muhammad Azeem on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RenderingEngine.h"
#import "Matrix3.h"
#import "Matrix4.h"

@interface RenderingEngineES2 : RenderingEngine
{
    Matrix4 _modelViewProjectionMatrix;
    Matrix3 _normalMatrix;
    
    GLuint _program;
    GLuint _skyboxProgram;
//    GLuint _program;
//    GLuint _vertexArray;
//    GLuint **_vertexBuffer;
}

@end
