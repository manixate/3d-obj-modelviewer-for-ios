//
//  RenderingEngineES2.m
//  ObjModelViewer
//
//  Created by Muhammad Azeem on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RenderingEngineES2.h"
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import "Object3D.h"
#import "Matrix3.h"
#import "ColorUtility.h"
#import "Cube.h"

@interface RenderingEngineES2 ()

//- (void)bindVBO;

- (BOOL)loadShaders;
- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file;
- (BOOL)linkProgram:(GLuint)prog;
- (BOOL)validateProgram:(GLuint)prog;

@end

@implementation RenderingEngineES2

- (id)init
{
    self = [super init];
    if (self)
    {
        //        glGenVertexArraysOES(1, &_vertexArray);
        //        glBindVertexArrayOES(_vertexArray);
        
        /*_vertexBuffer = malloc(1 * sizeof(GLuint));
         
         glGenBuffers(1, &_vertexBuffer[0]);
         glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer[0]);
         glBufferData(GL_ARRAY_BUFFER, sizeof(gCubeVertexData), gCubeVertexData, GL_STATIC_DRAW);
         
         [self loadShaders];
         
         glEnableVertexAttribArray(ATTRIB_VERTEX);
         glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, GL_FALSE, 32, BUFFER_OFFSET(0));
         glEnableVertexAttribArray(ATTRIB_NORMAL);
         glVertexAttribPointer(ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, 32, BUFFER_OFFSET(12));
         glEnableVertexAttribArray(ATTRIB_TEXTURE_COORD);
         glVertexAttribPointer(ATTRIB_TEXTURE_COORD, 2, GL_FLOAT, GL_FALSE, 32, BUFFER_OFFSET(24));*/
        
        [self loadShaders];
        
        //        _baseModelViewMatrix = Matrix4Identity;
    }
    
    return self;
}

- (id)initWithModelNames:(NSArray *)modelNames
{
    self = [super initWithModelNames:modelNames];
    if (self)
    {
        //        glGenVertexArraysOES(1, &_vertexArray);
        //        glBindVertexArrayOES(_vertexArray);
        
        /*_vertexBuffer = malloc(1 * sizeof(GLuint));
         
         glGenBuffers(1, &_vertexBuffer[0]);
         glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer[0]);
         glBufferData(GL_ARRAY_BUFFER, sizeof(gCubeVertexData), gCubeVertexData, GL_STATIC_DRAW);
         
         [self loadShaders];
         
         glEnableVertexAttribArray(ATTRIB_VERTEX);
         glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, GL_FALSE, 32, BUFFER_OFFSET(0));
         glEnableVertexAttribArray(ATTRIB_NORMAL);
         glVertexAttribPointer(ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, 32, BUFFER_OFFSET(12));
         glEnableVertexAttribArray(ATTRIB_TEXTURE_COORD);
         glVertexAttribPointer(ATTRIB_TEXTURE_COORD, 2, GL_FLOAT, GL_FALSE, 32, BUFFER_OFFSET(24));*/
        
        [self loadShaders];
        
        //        _baseModelViewMatrix = Matrix4MakeTranslation(0.0, 0.0, -6.0f);
        
        scaleFactor = 1.0;
        xRotation = yRotation = 0.0;
        xTranslation = yTranslation = zTranslation = 0.0;
        
        _vertexBuffer = malloc(scene3D.models.count * sizeof(GLuint*));
        for (int i = 0; i < scene3D.models.count; i++)
        {
            Model3D *model = [scene3D.models objectAtIndex:i];
            _vertexBuffer[i] = malloc(model.objects3D.count * sizeof(GLuint));
            
            glGenBuffers(model.objects3D.count, _vertexBuffer[i]);
        }
        
        for (int i = 0; i < scene3D.models.count; i++)
        {
            Model3D *model = [scene3D.models objectAtIndex:i];
            for (int j = 0; j < model.objects3D.count; j++)
            {
                
                Object3D *object3D = [model.objects3D objectAtIndex:j];
                glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer[i][j]);
                glBufferData(GL_ARRAY_BUFFER, object3D.completeDataSize * sizeof(GLfloat), object3D.completeData, GL_STATIC_DRAW);
                
                //            glEnableVertexAttribArray(ATTRIB_VERTEX);
                //            glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, GL_FALSE, 32, BUFFER_OFFSET(0));
                //            glEnableVertexAttribArray(ATTRIB_NORMAL);
                //            glVertexAttribPointer(ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, 32, BUFFER_OFFSET(12));
                //            glEnableVertexAttribArray(ATTRIB_TEXTURE_COORD);
                //            glVertexAttribPointer(ATTRIB_TEXTURE_COORD, 2, GL_FLOAT, GL_FALSE, 32, BUFFER_OFFSET(24));
                
                //            glBindBuffer(GL_ARRAY_BUFFER, 0);
                //            glBindVertexArrayOES(0);
            }
        }
    }
    
    return self;
}

- (id)initWithModelNames:(NSArray *)modelNames positions:(Vector3 *)positions scales:(Vector3 *)scales
{
    self = [super initWithModelNames:modelNames positions:positions scales:scales];
    if (self)
    {        
        [self loadShaders];
        
        scaleFactor = 1.0;
        xRotation = yRotation = 0.0;
        xTranslation = yTranslation = zTranslation = 0.0;
        
        _vertexBuffer = malloc(scene3D.models.count * sizeof(GLuint*));
        for (int i = 0; i < scene3D.models.count; i++)
        {
            Model3D *model = [scene3D.models objectAtIndex:i];
            _vertexBuffer[i] = malloc(model.objects3D.count * sizeof(GLuint));
            
            glGenBuffers(model.objects3D.count, _vertexBuffer[i]);
        }
        
        for (int i = 0; i < scene3D.models.count; i++)
        {
            Model3D *model = [scene3D.models objectAtIndex:i];
            for (int j = 0; j < model.objects3D.count; j++)
            {
                
                Object3D *object3D = [model.objects3D objectAtIndex:j];
                glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer[i][j]);
                glBufferData(GL_ARRAY_BUFFER, object3D.completeDataSize * sizeof(GLfloat), object3D.completeData, GL_STATIC_DRAW);
            }
        }
    }
    
    return self;
}

- (void)initFrameBuffer
{
    [super initFrameBuffer];
}

- (void)update:(NSTimeInterval)aTimeStamp
{
    [super update:aTimeStamp];
    
    CGRect bounds = [[UIScreen mainScreen] bounds];
    
    float aspect = fabsf(bounds.size.width / bounds.size.height);
    Matrix4 projectionMatrix = Matrix4MakePerspective(degreesToRadians(65.0f), aspect, 0.1f, 10000.0f);
    
    GLfloat totalRotation = sqrt(xRotation * xRotation + yRotation * yRotation);
    
    Matrix4 tempMatrix = Matrix4Identity;
    
    Matrix4 scalingMatrix = Matrix4MakeScale(scaleFactor, scaleFactor, scaleFactor);
    
    Matrix4 rotationMatrix = Matrix4MakeRotation(totalRotation * M_PI / 180, xRotation / totalRotation, yRotation / totalRotation, 0);
    //Matrix4MakeRotation(totalRotation * M_PI / 180, ((xRotation/totalRotation) * Matrix4Identity.m01 + (yRotation/totalRotation) * Matrix4Identity.m00), ((xRotation/totalRotation) * Matrix4Identity.m11 + (yRotation/totalRotation) * Matrix4Identity.m10), ((xRotation/totalRotation) * Matrix4Identity.m21 + (yRotation/totalRotation) * Matrix4Identity.m20));
    
    // Translate the model by the accumulated amount
	float currentScaleFactor = pow(Matrix4Identity.m00, 2.0f) + pow(Matrix4Identity.m01, 2.0f) + pow(Matrix4Identity.m02, 2.0f);
	
	xTranslation = xTranslation / currentScaleFactor;
	yTranslation = yTranslation / currentScaleFactor;
	zTranslation = zTranslation / currentScaleFactor;
    
    Matrix4 translationMatrix = Matrix4MakeTranslation(xTranslation * M_PI / 180, yTranslation * M_PI / 180, zTranslation);
    
    if ((scalingMatrix.m00 >= -100.0) && (scalingMatrix.m00 <= 100.0))
		tempMatrix = Matrix4Multiply(tempMatrix, scalingMatrix);
    if ((rotationMatrix.m00 >= -100.0) && (rotationMatrix.m00 <= 100.0))
		tempMatrix = Matrix4Multiply(rotationMatrix, tempMatrix);
    if ((translationMatrix.m00 >= -100.0) && (translationMatrix.m00 <= 100.0))
		tempMatrix = Matrix4Multiply(translationMatrix, tempMatrix);
    
    // Compute the model view matrix for the object rendered with it
    Matrix4 modelViewMatrix = Matrix4MakeTranslation(0, 0, -6.0);
    modelViewMatrix = Matrix4Multiply(modelViewMatrix, tempMatrix);
    
    _normalMatrix = Matrix3InvertAndTranspose(Matrix4GetMatrix3(modelViewMatrix), NULL);
    
    _modelViewProjectionMatrix = Matrix4Multiply(projectionMatrix, modelViewMatrix);
}

- (void)render
{
    [super render];
    
    // Draw Skybox
    glClearColor(0.5f, 0.5f, 0.5f, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(_skyboxProgram);
    
    glDisable(GL_DEPTH_TEST);   // skybox should be drawn behind anything else
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_textures.Cubemap);
    
    
    glBindBuffer(GL_ARRAY_BUFFER, _skyboxBuffer);
    glVertexAttribPointer(m_programSkyBox.Attributes.Position, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(m_programSkyBox.Attributes.Position);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skyBoxIndexBuffer);
    glDrawElements(GL_TRIANGLE_STRIP, 14, GL_UNSIGNED_BYTE, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glEnable(GL_DEPTH_TEST);
    
    // Draw Scene
    glEnable(GL_DEPTH_TEST);
    
    for (int i = 0; i < scene3D.models.count; i++)
    {
        Model3D *model3D = [scene3D.models objectAtIndex:i];
        
        Matrix4 modelPosition = Matrix4MakeTranslation(model3D.position.x, -model3D.position.y, model3D.position.z);
        Matrix4 modelScale = Matrix4MakeScale(model3D.scale.x, model3D.scale.y, model3D.scale.z);
        
        Matrix4 modelMatrix = Matrix4Identity;
        
        modelMatrix = Matrix4Multiply(_modelViewProjectionMatrix, modelScale);
        modelMatrix = Matrix4Multiply(modelMatrix, modelPosition);
        
        for (int j = 0; j < model3D.objects3D.count; j++)
        {
            glUseProgram(_program);
            
            Object3D *object3D = [model3D.objects3D objectAtIndex:j];
            
            glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer[i][j]);
            
            int offset = 0;
            glEnableVertexAttribArray(ATTRIB_VERTEX);
            glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, GL_FALSE, object3D.stride * 4, BUFFER_OFFSET(offset));
            
            if (object3D.hasTexCoords)
            {
                offset += 12;
                glEnableVertexAttribArray(ATTRIB_TEXTURE_COORD);
                glVertexAttribPointer(ATTRIB_TEXTURE_COORD, 2, GL_FLOAT, GL_FALSE, object3D.stride * 4, BUFFER_OFFSET(offset));
            }
            
            if (object3D.hasNormals)
            {
                offset += 8;
                glEnableVertexAttribArray(ATTRIB_NORMAL);
                glVertexAttribPointer(ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, object3D.stride * 4, BUFFER_OFFSET(offset));
            }
            
            glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX], 1, 0, modelMatrix.m);
            glUniformMatrix3fv(uniforms[UNIFORM_NORMAL_MATRIX], 1, 0, _normalMatrix.m);
            
            if (object3D.material)
            {
                glUniform4fv(uniforms[UNIFORM_AMBIENTCOLOR], 1, object3D.material->Ka);
                glUniform4fv(uniforms[UNIFORM_DIFFUSECOLOR], 1, object3D.material->Kd);
                glUniform4fv(uniforms[UNIFORM_SPECULARCOLOR], 1, object3D.material->Ks);
                
                glUniform1i(uniforms[UNIFORM_HASTEXTURE], object3D.material->hasTexture);
                
                if (object3D.material->hasTexture)
                {
                    glActiveTexture(GL_TEXTURE0); 
                    glBindTexture(GL_TEXTURE_2D, object3D.material->textureID);
                    glUniform1i(uniforms[UNIFORM_TEXTURE], 0);
                }
            }
            
            glDrawArrays(GL_TRIANGLES, 0, object3D.completeDataSize / object3D.stride);
            
            glDisableVertexAttribArray(ATTRIB_VERTEX);
            glDisableVertexAttribArray(ATTRIB_NORMAL);
            glDisableVertexAttribArray(ATTRIB_TEXTURE_COORD);
            
            glActiveTexture(GL_TEXTURE0); 
            glBindTexture(GL_TEXTURE_2D, 0);
            glDisable(GL_TEXTURE_2D);
        }
    }
    
    if (isMSAAEnabled == true)
    {
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER_APPLE, mFrameBuffer);
        glBindFramebuffer(GL_READ_FRAMEBUFFER_APPLE, mSampleFrameBuffer);
        glResolveMultisampleFramebufferAPPLE();
    }
}

#pragma mark -  OpenGL ES 2 shader compilation

- (BOOL)loadShaders
{
    GLuint vertShader, fragShader;
    NSString *vertShaderPathname, *fragShaderPathname;
    
    // Create shader program.
    _program = glCreateProgram();
    
    // Create and compile vertex shader.
    vertShaderPathname = [[NSBundle mainBundle] pathForResource:@"BumpShader" ofType:@"vsh"];
    if (![self compileShader:&vertShader type:GL_VERTEX_SHADER file:vertShaderPathname]) {
        NSLog(@"Failed to compile vertex shader");
        return NO;
    }
    
    // Create and compile fragment shader.
    fragShaderPathname = [[NSBundle mainBundle] pathForResource:@"BumpShader" ofType:@"fsh"];
    if (![self compileShader:&fragShader type:GL_FRAGMENT_SHADER file:fragShaderPathname]) {
        NSLog(@"Failed to compile fragment shader");
        return NO;
    }
    
    // Attach vertex shader to program.
    glAttachShader(_program, vertShader);
    
    // Attach fragment shader to program.
    glAttachShader(_program, fragShader);
    
    // Bind attribute locations.
    // This needs to be done prior to linking.
    glBindAttribLocation(_program, ATTRIB_VERTEX, "position");
    glBindAttribLocation(_program, ATTRIB_NORMAL, "normal");
    glBindAttribLocation(_program, ATTRIB_TEXTURE_COORD, "TexCoordIn");
    
    // Link program.
    if (![self linkProgram:_program]) {
        NSLog(@"Failed to link program: %d", _program);
        
        if (vertShader) {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader) {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (_program) {
            glDeleteProgram(_program);
            _program = 0;
        }
        
        return NO;
    }
    
    // Get uniform locations.
    uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX] = glGetUniformLocation(_program, "modelViewProjectionMatrix");
    uniforms[UNIFORM_NORMAL_MATRIX] = glGetUniformLocation(_program, "normalMatrix");
    uniforms[UNIFORM_TEXTURE] = glGetUniformLocation(_program, "Texture");
    uniforms[UNIFORM_AMBIENTCOLOR] = glGetUniformLocation(_program, "ambientColor");
    uniforms[UNIFORM_DIFFUSECOLOR] = glGetUniformLocation(_program, "diffuseColor");
    uniforms[UNIFORM_SPECULARCOLOR] = glGetUniformLocation(_program, "specularColor");
    uniforms[UNIFORM_HASTEXTURE] = glGetUniformLocation(_program, "hasTexture");
    
    // Release vertex and fragment shaders.
    if (vertShader) {
        glDetachShader(_program, vertShader);
        glDeleteShader(vertShader);
    }
    if (fragShader) {
        glDetachShader(_program, fragShader);
        glDeleteShader(fragShader);
    }
    
    return YES;
}

- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file
{
    GLint status;
    const GLchar *source;
    
    source = (GLchar *)[[NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil] UTF8String];
    if (!source) {
        NSLog(@"Failed to load vertex shader");
        return NO;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0) {
        glDeleteShader(*shader);
        return NO;
    }
    
    return YES;
}

- (BOOL)linkProgram:(GLuint)prog
{
    GLint status;
    glLinkProgram(prog);
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
#endif
    
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

- (BOOL)validateProgram:(GLuint)prog
{
    GLint logLength, status;
    
    glValidateProgram(prog);
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program validate log:\n%s", log);
        free(log);
    }
    
    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

-(void)dealloc
{
    glDeleteShader(_program);
    //    glDeleteBuffers(model3D.objects3D.count, _vertexBuffer);
    //    glDeleteVertexArraysOES(1, &_vertexArray);
}

@end