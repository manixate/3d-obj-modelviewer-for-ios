//
//  Scene.h
//  3DModelViewer
//
//  Created by Muhammad Azeem on 8/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vector3.h"

@interface Scene : NSObject
{
    NSMutableArray *models;
}
@property (nonatomic) NSMutableArray *models;

- (id)initWithModelNames:(NSArray *)modelNames;
- (id)initWithModelNames:(NSArray *)modelNames positions:(Vector3 *)positions andScale:(Vector3 *)scales;

@end
