//
//  Scene.m
//  3DModelViewer
//
//  Created by Muhammad Azeem on 8/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Scene.h"
#import "Model3D.h"
#import "ObjLoaderC.h"

@implementation Scene
@synthesize models;

- (id)init
{
    self = [super init];
    if (self) {
        models = [NSMutableArray array];
    }
    return self;
}

- (id)initWithModelNames:(NSArray *)modelNames
{
    self = [super init];
    if (self)
    {
        models = [NSMutableArray array];
        
        for (NSString *modelName in modelNames)
        {
            NSString *path = [[NSBundle mainBundle] pathForResource:modelName ofType:@"obj"];
            ObjLoaderC *objLoader = [[ObjLoaderC alloc]init];
            
            Model3D *model3D = [objLoader loadObj:path];
            model3D.modelName = modelName;
            
            [model3D normalizeModel];
            
            [models addObject:model3D];
        }
    }
    return self;
}

- (id)initWithModelNames:(NSArray *)modelNames positions:(Vector3 *)positions andScale:(Vector3 *)scales
{
    self = [super init];
    if (self)
    {
        models = [[NSMutableArray alloc] init];
        
        int i = 0;
        for (NSString *modelName in modelNames)
        {
            NSString *path = [[NSBundle mainBundle] pathForResource:modelName ofType:@"obj"];
            ObjLoaderC *objLoader = [[ObjLoaderC alloc]init];
            
            Model3D *model3D = [objLoader loadObj:path];
            model3D.position = positions[i];
            model3D.scale = scales[i];
            model3D.modelName = modelName;
            
            [model3D normalizeModel];
            
            [models addObject:model3D];
            
            i++;
        }
    }
    return self;
}

@end
