//
//  Shader.fsh
//  BumpMap
//
//  Created by Muhammad Azeem on 7/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

varying lowp vec4 colorVarying;

void main()
{
    gl_FragColor = colorVarying;
}
