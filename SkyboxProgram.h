//
//  SkyboxProgram.h
//  3DModelViewer
//
//  Created by Muhammad Azeem on 9/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

// Uniform index.
enum
{
    UNIFORM_MODELVIEWPROJECTION_MATRIX,
    UNIFORM_NORMAL_MATRIX,
    UNIFORM_TEXTURE,
    UNIFORM_AMBIENTCOLOR,
    UNIFORM_DIFFUSECOLOR,
    UNIFORM_SPECULARCOLOR,
    UNIFORM_HASTEXTURE,
    NUM_UNIFORMS
};
GLint uniforms[NUM_UNIFORMS];

// Attribute index.
enum
{
    ATTRIB_VERTEX,
    ATTRIB_NORMAL,
    ATTRIB_TEXTURE_COORD,
    NUM_ATTRIBUTES
};

@interface SkyboxProgram : NSObject

@end
