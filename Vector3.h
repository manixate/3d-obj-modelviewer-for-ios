//
//  Vector3.h
//  ObjModelViewer
//
//  Created by Muhammad Azeem on 7/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef ObjModelViewer_Vector3_h
#define ObjModelViewer_Vector3_h

union _Vector3
{
    struct { float x, y, z; };
    struct { float r, g, b; };
    struct { float s, t, p; };
    float v[3];
};
typedef union _Vector3 Vector3;

static Vector3 Vector3Make(float x, float y, float z)
{
    Vector3 v = { x, y, z };
    return v;
}

static float Vector3Length(Vector3 vector)
{
    return sqrt(vector.v[0] * vector.v[0] + vector.v[1] * vector.v[1] + vector.v[2] * vector.v[2]);
}

static Vector3 Vector3Normalize(Vector3 vector)
{
    float scale = 1.0f / Vector3Length(vector);
    Vector3 v = { vector.v[0] * scale, vector.v[1] * scale, vector.v[2] * scale };
    return v;
}

#endif
